import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes } from '@angular/router';
import { LoginComponent } from './content/login/login.component';
import { PortalComponent } from './content/portal/portal.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: PortalComponent,
    children: [
      {
        path: '',
        loadChildren: 'src/app/content/portal/portal.module#PortalModule',
      }
    ],
  },
  {
    path: 'login',
    component: LoginComponent,

  },
  {
    path: 'admin',
    loadChildren: 'src/app/content/admin/admin.module#AdminModule',
  },
];
