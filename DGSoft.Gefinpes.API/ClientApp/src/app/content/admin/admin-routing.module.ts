import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { Teste2Component } from './teste2/teste2.component';

export const AdminRoutes: Routes = [
  {
    path: '',
    component: MainComponent
  },
  {
    path: 'teste',
    component: Teste2Component
  },
];

