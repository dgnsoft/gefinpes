import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainComponent } from './main/main.component';
import { Teste2Component } from './teste2/teste2.component';
import { RouterModule } from '@angular/router';
import { AdminRoutes } from './admin-routing.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminRoutes),
  ],
  declarations: [MainComponent, Teste2Component]
})
export class AdminModule { }
