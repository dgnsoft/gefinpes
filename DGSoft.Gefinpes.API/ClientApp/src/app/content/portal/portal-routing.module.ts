import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortalComponent } from './portal.component';
import { AboutComponent } from './about/about.component';
import { PricingComponent } from './pricing/pricing.component';
import { BlogComponent } from './blog/blog.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { ContactComponent } from './contact/contact.component';

export const PortalRoutes: Routes = [{
    path: '',
    component: AboutComponent,
},
{
    path: 'about',
    component: AboutComponent,

}, {
    path: 'pricing',
    component: PricingComponent,

}, {
    path: 'blog',
    component: BlogComponent,

}, {
    path: 'portfolio',
    component: PortfolioComponent,

}, {
    path: 'contact',
    component: ContactComponent,

}];
