import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { PortalRoutes } from './portal-routing.module';
import { SharedModule } from '@shared/shared.module';
import { ContactDialogComponent } from './contact-dialog/contact-dialog.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { BlogComponent } from './blog/blog.component';
import { HeadingComponent } from './heading/heading.component';
import { PricingComponent } from './pricing/pricing.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { TesteComponent } from './teste/teste.component';
import { PortalComponent } from './portal.component';

@NgModule({
  declarations: [
    ContactComponent,
    AboutComponent,
    BlogComponent,
    HeadingComponent,
    PricingComponent,
    PortfolioComponent,
    TesteComponent,
    ContactDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(PortalRoutes)
  ],
  entryComponents: [
    ContactDialogComponent
  ],
  exports: [

  ],

})
export class PortalModule { }
