
import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatCheckboxModule, MatCardModule, MatDividerModule, MatIconModule,
  MatSidenavModule, MatToolbarModule, MatListModule, MatTooltipModule, MatTabsModule, MatTableModule,
  MatFormFieldModule, MatInputModule, MatDialogModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { nglpScrollDirective } from './directives/scroll.directive';
import { AboutDirective } from './directives/about.directive';

@NgModule({

  imports: [CommonModule, MatButtonModule, MatCheckboxModule, MatCardModule, MatDividerModule, MatIconModule,
    MatSidenavModule, MatToolbarModule, MatListModule, MatTooltipModule, MatTabsModule,
    MatTableModule, MatFormFieldModule, MatInputModule, MatDialogModule, FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule],
  declarations: [
    nglpScrollDirective,
    AboutDirective
  ],
  exports: [CommonModule, MatButtonModule, MatCheckboxModule, MatCardModule, MatDividerModule, MatIconModule,
    MatSidenavModule, MatToolbarModule, MatListModule, MatTooltipModule, MatTabsModule,
    MatTableModule, MatFormFieldModule, MatInputModule, MatDialogModule, FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    nglpScrollDirective,
    AboutDirective
  ]
})
export class SharedModule { }
