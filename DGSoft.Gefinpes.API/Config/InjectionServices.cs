﻿using DGSoft.Gefinpes.Service;
using Domain.BLL.Interfaces;
using Domain.Models;
using Microsoft.Extensions.DependencyInjection;

namespace DGSoft.Gefinpes.API.Config
{
    public static class InjectionServices
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public static void Configure(this IServiceCollection services)
        {
            services.AddTransient<IGenericService<Menu>, MenuService>();
            services.AddTransient<IGenericService<Profile>, ProfileService>();
        }
    }
}
