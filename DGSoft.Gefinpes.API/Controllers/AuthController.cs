﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.BLL.Intefaces;
using Domain.BLL.Interfaces;
using Domain.DAL.Service;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DGSoft.Gefinpes.Domain.Models.Core;

namespace EfcoreSample.Controllers
{
   
    /// <summary>
    /// Controller to authentication operations
    /// </summary>
    [ApiController]
    [Route("api/[controller]/")]
    
    public class AuthController : ControllerBase       
    {
        private readonly IAuthService _authService;

        /// <summary>
        /// Constructor for Controller. Inject Auth service to use on methods
        /// </summary>
        /// <param name="_authService"></param>
        public AuthController(IAuthService _authService)
        {
            this._authService = _authService;
        }

        /// <summary>
        /// Check authentication credentials and generate token for oauth
        /// </summary>
        /// <param name="auth"></param>
        /// <returns></returns>
        [HttpPost("login")]
        public ActionResult<ResponseModel<UserDTO>> PostAuth(AuthDTO auth)
        {
            ResponseModel<UserDTO> resp = new ResponseModel<UserDTO>();
            try
            {
                UserDTO u = this._authService.Authenticate(auth.Login, auth.Password);
                resp.Result.Add(u);
                return resp;
            }
            catch (Exception e)
            {
                string error = e.Message;
                resp.Success = false;
                resp.Message.Add(error);
                return BadRequest(error);
            }
        }

        /// <summary>
        /// Find a Custommer user in database by login and return information from application to redirect to login application page.
        /// </summary>
        /// <param name="auth"></param>
        /// <returns></returns>
        [HttpPost("user/custommer/info")]
        public ActionResult<ResponseModel<UserCustommerDTO>> PostUserByLogin(AuthDTO auth)
        {
            ResponseModel<UserCustommerDTO> resp = new ResponseModel<UserCustommerDTO>();
            try
            {
                UserCustommerDTO u = this._authService.GetUserCustommerDataByLogin(auth.Login);
                resp.Result.Add(u);
                return Ok(resp);
            }
            catch (Exception e)
            {
                string error = e.Message;
                resp.Message.Add(error);
                resp.Success = false;                
                return BadRequest(resp);
            }
        }

        /// <summary>
        ///  Authenticate Custommer user and return data from user to application
        /// </summary>
        /// <param name="auth"></param>
        /// <returns></returns>
        [HttpPost("user/custommer/login")]
        public ActionResult<ResponseModel<UserCustommerFullDTO>> PostUserCostummerLogin(AuthDTO auth)
        {            
            var headers = Request.Headers;

            //Create a Param on admin to put the autorized urls to consume validate server 
            //address authorized to catch informations
            var OriginAddress = headers["Origin"];
            var referer = headers["Referer"];
            var host = headers["Host"];

            ResponseModel<UserCustommerFullDTO> resp = new ResponseModel<UserCustommerFullDTO>();
            try
            {
                UserCustommerFullDTO u = this._authService.AutenticateUserCustommer(auth.Login, auth.Password);
                resp.Result.Add(u);
                return resp;
            }
            catch (Exception e)
            {
                string error = e.Message;
                resp.Errors.Add(error);
                resp.Success = false;                
                return BadRequest(resp);
            }
        }



        [HttpPost("/user/logged/name")]
        [Authorize]
        public ActionResult<ResponseModel<string>> GetLoggedUser()
        {
            ResponseModel<string> resp = new ResponseModel<string>();
            try
            {
                String userData = this._authService.GetLoggedUserName(User);
                resp.Result.Add(userData);
                return Ok(resp);
            }
            catch (Exception e)
            {
                string error = e.Message;
                resp.Success = false;
                resp.Errors.Add(error);
                return BadRequest(resp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("/user/logged/menus")]
        [Authorize]
        public ActionResult<ResponseModel<Menu>> GetUserLoggedMenus()
        {
            ResponseModel<Menu> resp = new ResponseModel<Menu>();
            try
            {
                var userData = this._authService.GetLoggedUserData(User);
                List<Menu> result = this._authService.GetMenusForProfileLoggedUser(userData.ProfileId);
                resp.Result = result;
                return Ok(resp) ;
            }
            catch (Exception e)
            {
                string error = e.Message;
                resp.Success = false;
                resp.Errors.Add(error);
                return BadRequest(resp);
            }
        }

        [HttpPost("/user/logged/id")]
        [Authorize]
        public ActionResult<ResponseModel<long>> GetLoggedUserId()
        {
            ResponseModel<long> resp = new ResponseModel<long>();
            try
            {
                long userData = this._authService.GetLoggedUserId(User);
                resp.Result.Add(userData);
                return Ok(resp);
            }
            catch (Exception e)
            {
                string error = e.Message;
                resp.Success = false;
                resp.Errors.Add(error);
                return BadRequest(resp);
            }
        }

        [HttpPost("/user/logged/data")]
        [Authorize]
        public ActionResult<ResponseModel<UserDTO>> GetLoggedUserData()
        {
            ResponseModel<UserDTO> resp = new ResponseModel<UserDTO>();
            try
            {
                UserDTO userData = this._authService.GetLoggedUserData(User);
                resp.Result.Add(userData);
                return Ok(resp);
            }
            catch (Exception e)
            {
                string error = e.Message;
                resp.Success = false;
                resp.Errors.Add(error);
                return BadRequest(resp);
            }
        }



    }
}
