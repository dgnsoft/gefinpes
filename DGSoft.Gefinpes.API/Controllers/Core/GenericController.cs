﻿using System;
using System.Collections.Generic;
using System.Linq;
using DGSoft.Gefinpes.API.Util;
using DGSoft.Gefinpes.Domain.Models.Core;
using Domain.BLL;
using Domain.BLL.Intefaces;
using Domain.BLL.Interfaces;
using Domain.Core.Util;
using Domain.DAL.Repository.Interfaces;
using Domain.DAL.Service;
using Domain.Models;
using Domain.Models.Interfaces;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;
using DGSoft.Gefinpes.Core;


namespace DGSofp.Api.Controllers
{
    /// <summary>
    /// Generic controller for Crud Base Operations
    /// </summary>
    /// <typeparam name="T"></typeparam>

    [ApiController]
    [Route("api/[controller]")]
    public class GenericController<T> : ControllerBase
        where T : EntityBase
    {

        private readonly AuthService _authService = new AuthService();

        /// <summary>
        /// Generate Result 
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        protected ActionResult<ResponseModel<T>> GenerateResult(ResponseModel<T> response)
        {
            if (response.Success)
            {
                return Ok(response);
            }
            switch (response.ExceptionLevel)
            {
                case DGSoft.Gefinpes.Domain.Enums.Response.ExceptionLevel.NoDataFound:
                    return NotFound(response);
                case DGSoft.Gefinpes.Domain.Enums.Response.ExceptionLevel.ValidationError:
                    return BadRequest(response);
                case DGSoft.Gefinpes.Domain.Enums.Response.ExceptionLevel.InternalError:
                default:
                    return StatusCode(500, response);
            }
        }
        /// <summary>
        /// Find a Record by Id
        /// </summary>
        /// <param name="service"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [CustomAuthorize]
        public virtual ActionResult<ResponseModel<T>> GetById([FromServices] IGenericService<T> service, long id)
        {
            ResponseModel<T> result = new ResponseModel<T>();
            try
            {
                result = service.GetById(id);
                if (result.Result == null || result.Result.Count == 0)
                {
                    result.ExceptionLevel = DGSoft.Gefinpes.Domain.Enums.Response.ExceptionLevel.NoDataFound;                    
                }
            }
            catch (Exception e)
            {
                result = new ResponseModel<T>();
                string error = e.Message;
                result.Success = false;
                result.Errors.Add(error);
            }
            return GenerateResult(result);
        }
        /// <summary>
        ///  Get all itens 
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public virtual ActionResult<ResponseModel<T>> Get([FromServices] IGenericService<T> service)
        {
            ResponseModel<T> result = new ResponseModel<T>();
            try
            {
                result = service.Filter();               
            }
            catch (Exception e)
            {
                result = new ResponseModel<T>();
                string error = e.Message;
                result.Success = false;
                result.Message.Add(error);
            }
            return GenerateResult(result);
        }
        
        /*
        /// <summary>
        /// Update a Record
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [CustomAuthorize]
        public ActionResult<ApiResponse<TEntity>> Put(long id, TEntity entity)
        {
            ApiResponse<TEntity> resp = new ApiResponse<TEntity>();

            ICrudService<TEntity, TSearchDTO> service = new CrudService<TEntity, TSearchDTO, TRepository>(this._authService.GetLoggedUserDatabaseName(User));
            //Set logged user data for log
            service.SetUserLoggedData(this._authService.GetLoggedUserData(User));
            
            if (id != entity.Id)
            {
                return BadRequest("Id for entity is diferent from ID Object");
            } else { }

            if (!ModelState.IsValid)
            {
                resp.Errors = HttpUtil.GetErrorsFromModelstate(ModelState);
                resp.Success = false;
                return BadRequest(resp); //Melhorar esse retorno posteriomente
            } else { }
            try
            {               
                service.Edit(id, entity);
                TEntity entityAlt = service.GetById(id);
                resp.Data = entityAlt;
            }
            catch (Exception e)
            {
                string error = e.Message;
                resp.Success = false;
                resp.Errors.Add(error);
                return BadRequest(resp);
            }
            return Ok(resp);

        }

        /// <summary>
        /// Create a record
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        [CustomAuthorize]
        public ActionResult<TEntity> Post(TEntity entity)
        {
            ApiResponse<TEntity> resp = new ApiResponse<TEntity>();

            ICrudService<TEntity, TSearchDTO> service = new CrudService<TEntity, TSearchDTO, TRepository>(this._authService.GetLoggedUserDatabaseName(User));

            //Set logged user data for log
            service.SetUserLoggedData(this._authService.GetLoggedUserData(User));

            if (!ModelState.IsValid)
            {
                resp.Errors = HttpUtil.GetErrorsFromModelstate(ModelState);
                resp.Success = false;
                return BadRequest(resp); //Melhorar esse retorno posteriomente
            }
            try
            {
                service.Create(entity);
                TEntity entityCreated = service.GetById(entity.Id);
                resp.Data = entityCreated;
                return Ok(resp);
            }
            catch (Exception e)
            {
                string error = e.Message;
                resp.Success = false;
                resp.Errors.Add(error);
                return BadRequest(resp);
            }
        }

        /// <summary>
        /// Delete a record from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Languages1/5
        [HttpDelete("{id}")]
        //[CustomAuthorize]
        [Authorize]
        public ActionResult<TEntity> Delete(long id)
        {
            ApiResponse<TEntity> resp = new ApiResponse<TEntity>();

            ICrudService<TEntity, TSearchDTO> service = new CrudService<TEntity, TSearchDTO,  TRepository>(this._authService.GetLoggedUserDatabaseName(User));            
            try
            {
                var language = service.GetById(id);
                service.DeleteById(id);
            }
            catch (Exception e)
            {
                string error = e.Message;
                resp.Success = false;
                resp.Errors.Add(error);
                return BadRequest(resp);
            }

            return Ok(resp);
        }

        /// <summary>
        /// Generic search method for cruc basic
        /// </summary>
        /// <param name="searchEntity"></param>
        /// <returns></returns>
        [HttpPost("search")]
        [CustomAuthorize]
        //[Authorize]
        public ActionResult<TEntity> PostSearch(TSearchDTO searchEntity)
        {
            ApiResponse<TEntity> resp = new ApiResponse<TEntity>();

            try
            {
                ICrudService<TEntity , TSearchDTO > service = new CrudService<TEntity, TSearchDTO, TRepository>(this._authService.GetLoggedUserDatabaseName(User));
                List<TEntity> result = service.Search(searchEntity).ToList();
                resp.ListData = result;
                return Ok(resp);
            }
            catch (Exception e)
            {
                string error = e.Message;
                resp.Success = false;
                resp.Errors.Add(error);
                return BadRequest(resp);
            }
        }*/
    }
}
