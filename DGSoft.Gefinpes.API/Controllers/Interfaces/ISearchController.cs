﻿using DGSoft.Gefinpes.Domain.Models.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DGSoft.Gefinpes.Controllers.Interfaces
{
    /// <summary>
    /// Interface para exigir a implmentação de um método Search no controller de crud.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface ISearchController<TEntity>
    {
       /// <summary>
       /// O Método que será implementado deverá ser um método Post a url de acesso aos dados 'search'.
       /// Deverá receber como parametro um objeto com os dados que será feito a pesquisa.
       /// E a lógica deverá ser implementada conforme os requisitos de pesquisa da página.
       /// 
       /// Anotação do método : [HttpPost("search")]
       /// </summary>
       /// <param name="entity"></param>
       /// <returns></returns>
        ActionResult<ResponseModel<TEntity>> Search(TEntity entity);
    }
}
