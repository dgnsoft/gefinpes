﻿using DGSofp.Api.Controllers;
using Domain.Core;
using Domain.DAL.Repository;
using Domain.Models;
using Domain.Models.DTO;
using Microsoft.AspNetCore.Mvc;
using DGSoft.Gefinpes.Controllers.Interfaces;
using DGSoft.Gefinpes.Core;
using DGSoft.Gefinpes.Core.Attributes;

namespace EfcoreSample.Controllers
{
    
    /// <summary>
    /// Controller for Table Profiles
    /// </summary>
    [MenuId(Constants.ID_MENU_PROFILE)]
    public class ProfilesController : GenericController<Profile>
    {
        public static long _MenuId = Constants.ID_MENU_PROFILE;
        public ProfilesController()
        {

        }
    }
}
