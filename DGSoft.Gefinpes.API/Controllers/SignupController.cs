﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.BLL;
using Domain.BLL.Interfaces;
using Domain.DAL;
using Domain.Core;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Domain.Core.Util;
using DGSoft.Gefinpes.Core;
using System.Net;
using DGSoft.Gefinpes.API.Util;
using DGSoft.Gefinpes.Domain.Models.Core;

namespace EfcoreSample.Controllers
{
    /// <summary>
    /// Controller to user
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class SignupController : ControllerBase
    {
        private readonly ISignupRequestService _signupService;

        /// <summary>
        /// Constructor for Signup Controller
        /// </summary>
        /// <param name="signupService"></param>
        public SignupController(ISignupRequestService signupService)
        {
            this._signupService = signupService;
        }

        /// <summary>
        /// Method to create new Signup Request to new Custommer 
        /// </summary>
        /// <param name="signupRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<ResponseModel<SignupRequest>> PostSignup(SignupRequest signupRequest)
        {
            ResponseModel<SignupRequest> resp = new ResponseModel<SignupRequest>();

            if (!ModelState.IsValid)
            {
                resp.Message = HttpUtil.GetErrorsFromModelstate(ModelState);
                resp.Success = false;
                return BadRequest(resp); //Melhorar esse retorno posteriomente
            }
            try
            {
                SignupRequest signup = this._signupService.Create(signupRequest);
                resp.Result.Add(signup);
                resp.Success = true;
                return Ok(resp);
            }
            catch (Exception e)
            {
                string error = e.Message;
                resp.Success = false;
                resp.Message.Add(error);
                return BadRequest(resp);
            }
        }

        /// <summary>
        /// Method to confirm signup and efetivate creation of Tenant custommer instance
        /// </summary>
        /// <param name="signupKey"></param>
        /// <returns></returns>
        [HttpGet("confirm/{signupKey}")]
        public ActionResult<ResponseModel<SignupRequest>> GetSignupConfirm(string signupKey)
        {
            ResponseModel<SignupRequest> resp = new ResponseModel<SignupRequest>();
            try
            {
                if (String.IsNullOrEmpty(signupKey))
                {
                    throw new Exception("Signupkey é obrigatório na confirmação!");
                } else
                {
                    if (this._signupService.ConfirmSignup(signupKey))
                    {
                        resp.Success = true;
                        string msg = "Inscrição realizada com sucesso! Em breve você recerá um email com o link para acessar a aplicação.";
                        return Ok(msg);
                    } else
                    {
                        resp.Success = false;
                        resp.Message.Add("Ocorreu um erro ao tentar ativar sua conta!");
                        return BadRequest(resp);
                    }                    
                }
            }
            catch (Exception e)
            {
                string error = e.Message;
                resp.Success = false;
                resp.Message.Add(error);
                return BadRequest(resp);
            }
        }
    }
  }
