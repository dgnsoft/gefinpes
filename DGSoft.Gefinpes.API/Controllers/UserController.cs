﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.BLL;
using Domain.BLL.Interfaces;
using Domain.DAL;
using Domain.Core;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Domain.Core.Util;
using DGSoft.Gefinpes.Core;

namespace EfcoreSample.Controllers
{
    /// <summary>
    /// Controller to user
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class CoreController : ControllerBase
    {
        private readonly IDBService _dbservice;

        public CoreController(IDBService dbservice)
        {
            this._dbservice = dbservice;
        }

        [CustomAuthorize]
        [HttpGet("check/{database}/IsDatabaseLastVersion")]
        public IActionResult Get(string database)
        {
            try
            {
                bool result = _dbservice.IsDatabaseLastVersion(database);

                return Ok(result);
            }
            catch (Exception e)
            {
                string error = e.Message;
                return BadRequest(error);
            }

        }

        /// <summary>
        /// Method to migrate DB Verision
        /// </summary>
        /// <param name="database"></param>
        /// <returns></returns>
        [HttpGet("migrate/{database}")]
        //[CustomAuthorize(_menu)]
        public IActionResult GetMigrateDatabase(string database)
        {
            try
            {
                bool result = _dbservice.MigrateToLastVersion(database);

                return Ok(result);
            }
            catch (Exception e)
            {
                string error = e.Message;
                return BadRequest(error);
            }
        }

        /// <summary>
        /// Encrypt a value encrypted by system configured encryption method
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost("encrypt/text")]
        public IActionResult PostEncryptValue(string value)
        {
            try
            {
                string result = CryptoUtil.EncryptText(value);

                return Ok(result);
            }
            catch (Exception e)
            {
                string error = e.Message;
                return BadRequest(error);
            }
        }

        /// <summary>
        /// Decrypt a value encrypted by system configured encryption method
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost("decrypt/text")]
        public IActionResult PostDecryptValue(string value)
        {
            try
            {
                string result = CryptoUtil.DecryptText(value);

                return Ok(result);
            }
            catch (Exception e)
            {
                string error = e.Message;
                return BadRequest(error);
            }
        }

        /// <summary>
        /// Teste send email by application
        /// </summary>
        /// <param name="email"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        [HttpPost("send/mail")]
        public IActionResult PostSendEmail(Email email)
        {
            try
            {
                IMessageService _messageService = new EmailService();
                bool result = _messageService.Send(email.subject, email.content, email.toAddresss);
                return Ok(result);
            }
            catch (Exception e)
            {
                string error = e.Message;
                return BadRequest(error);
            }
        }

        [HttpPost("test/removeAccenpts")]
        public IActionResult PostRemoveAccenpts(String text)
        {
            try
            {               
                string result = StringUtils.RemoveAcentuation(text);
                return Ok(result);
            }
            catch (Exception e)
            {
                string error = e.Message;
                return BadRequest(error);
            }
        }

        [HttpPost("test/validatePassword")]
        public IActionResult PostValidatePassword(String text)
        {
            try
            {
                bool result = StringUtils.IsValidPassword(text);
                return Ok(result);
            }
            catch (Exception e)
            {
                string error = e.Message;
                return BadRequest(error);
            }
        }

        public class Email
        {
            public string subject { get; set; }
            public string content { get; set; }
            public List<String> toAddresss { get; set; }

        }  
        
    }
}
