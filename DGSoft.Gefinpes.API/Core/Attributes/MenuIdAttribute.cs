﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DGSoft.Gefinpes.Core.Attributes
{
    [System.AttributeUsage(System.AttributeTargets.Class |
                         System.AttributeTargets.Struct | System.AttributeTargets.Property)]
    public class MenuId : System.Attribute
    {
        public long _id;

        public MenuId(long id)
        {
            this._id = id;
        }
    }
}
