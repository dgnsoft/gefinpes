﻿using Domain.BLL;
using Domain.BLL.Intefaces;
using Domain.Core.Util;
using Domain.Models;
using Domain.Models.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using DGSoft.Gefinpes.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace DGSoft.Gefinpes.Core
{
    /// <summary>
    /// Custom implementation for Authorize access to controller method based on Access control for application
    /// </summary>
    [AttributeUsageAttribute(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class CustomAuthorizeAttribute : AuthorizeAttribute, IAuthorizationFilter
    { 
        /// <summary>
        /// Id for functionality to make validations on access control
        /// </summary>
       public long? FunctionalityId { get; set; }

        /// <summary>
        /// Valiadate the permissions
        /// </summary>
        /// <param name="context"></param>
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            string searchMethodName = "PostSearch"; //Name for method default for search operation on crud base
            var user = context.HttpContext.User;

            if (!user.Identity.IsAuthenticated)
            {
                context.Result = new StatusCodeResult((int)System.Net.HttpStatusCode.Unauthorized);
                return;
            }
            var attributes = context.ActionDescriptor.GetType().GetCustomAttributes(true);

            bool isAuthorized = true;

            // you can also use registered services
            //var someService = context.HttpContext.RequestServices.GetService<ISomeService>();

            //instatiate authservice objects to verify permissions
            IAuthService auth = new AuthService();
            UserDTO userLogged = auth.GetLoggedUserData(user);

            string httpVerb = context.HttpContext.Request.Method.ToUpper();
            // string httpBody = StringUtils.GetStringValueFromReader(context.HttpContext.Request.Body);
            var controllerActionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
            if (controllerActionDescriptor != null)
            {
                //Validate if is a permission on functionatily, 
                //if Yes, validation permission from functionality geting menuId on funcionality, if has view permission
                //verify permission for funcionationality on menu
                if (FunctionalityId.HasValue && FunctionalityId.Value>0)
                {
                    ProfileMenuFuncionality permissionFunctionality = auth.GetProfilemMenuFunctionalityPermission(userLogged.DatabaseName,
                                                                                 userLogged.ProfileId,
                                                                                 FunctionalityId.Value);

                    //If has permission on functionality
                    if (permissionFunctionality.Granted)
                    {                        
                        //Verify if has permission to view menu page
                        ProfileMenu permissionMenu = auth.GetProfileMenuPermission(userLogged.DatabaseName,
                                                                                  userLogged.ProfileId,
                                                                                  permissionFunctionality.MenuFuncionality.MenuId);
                        //if has no permission to view menu
                        if (!permissionMenu.View)
                        {
                            //has no authorize
                            isAuthorized = false;
                        } else
                        {
                           
                        }
                    } else
                    {
                        //has no authorize
                        isAuthorized = false;
                    }                    
                } else
                {
                    var actionAttributes = controllerActionDescriptor.ControllerTypeInfo.GetCustomAttributes(true);
                    var MenuAttribute = actionAttributes.Where(p => p.GetType() == typeof(MenuId)).FirstOrDefault();
                    if (MenuAttribute != null)
                    {

                        var MenuId = ((MenuId)MenuAttribute)._id;
                        if (MenuId > 0)
                        {

                            ProfileMenu permission = auth.GetProfileMenuPermission(userLogged.DatabaseName,
                                                                                   userLogged.ProfileId,
                                                                                   MenuId);
                            if (permission == null || !permission.View)
                            {
                                isAuthorized = false;
                            }
                            else
                            {

                                switch (httpVerb)
                                {
                                    case "GET":
                                        isAuthorized = permission.View;
                                        break;
                                    case "POST":
                                        //Get method called from controller
                                        var methodCalled = controllerActionDescriptor.MethodInfo.Name;

                                        //If methodCalledName = PostSearch
                                        if (searchMethodName.Equals(methodCalled))
                                        {
                                            //It's calling Search, and permission is the same from view page
                                            isAuthorized = permission.View;
                                        }
                                        else
                                        {
                                            //Permission it's from create
                                            isAuthorized = permission.Create;
                                        }

                                        break;
                                    case "PUT":
                                        isAuthorized = permission.Edit;
                                        break;
                                    case "DELETE":
                                        isAuthorized = permission.Delete;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        else
                        {
                            // if MenuAttribute == null implementation comes here
                        }
                    }
                    else { 
                    
                    }
                }                
            }

            if (!isAuthorized)
            {
                context.Result = new StatusCodeResult((int)System.Net.HttpStatusCode.Unauthorized);
                return;
            }
        }
    }
}
