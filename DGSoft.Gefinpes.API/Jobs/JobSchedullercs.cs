﻿using FluentScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DGSoft.Gefinpes.Jobs
{
    /// <summary>
    /// Class to configure Schedulled Jobs
    /// </summary>
    public class JobScheduller
    {
        /// <summary>
        /// Job Scheduling
        /// </summary>
        public static void StartJobs()
        {
            //Add WeekdaysOnly to not run on Weekends
            JobManager.AddJob(() => JobSendMessages.Execute(), s => s.WithName("ENVIO_EMAILS")
                 .ToRunNow().AndEvery(1).Minutes());
        }
    }
}
