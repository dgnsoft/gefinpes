﻿using Domain.BLL.Interfaces;
using Domain.Core;
using Domain.Core.Util;
using Domain.DAL;
using Domain.Models;
using FluentScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Domain.Core.Enums;

namespace DGSoft.Gefinpes.Jobs
{
    /// <summary>
    /// Job to Send messages pending
    /// </summary>
    public class JobSendMessages
    {
        /// <summary>
        /// Execution to method
        /// </summary>
        public static void Execute()
        {
            try
            {
                DateTime dateInit = DateTime.Now;
                PersistentContext _context = new PersistentContext();

                //Get configured messages types
                List<MessageType> messageTypes = _context.MessageTypes.ToList();

                //For each message type will sent messages
                foreach(MessageType type in messageTypes)
                {
                    //Instantiate by reflection the service that will provide to sent messages
                    string MessageServiceFullName = type.ServiceImpFullName;
                    var serviceObj = ReflectionUtils.GetInstance(MessageServiceFullName);

                    IMessageService service  = (IMessageService)serviceObj;

                    if (service != null)
                    {                    
                        //Get the pending messages 
                        List<Message> pendinMessages = _context.Messages.Where(p => p.MessageTypeId == type.Id && p.SendMessageStatus == SendMessageStatus.Pending).ToList();

                        //For each pending message
                        foreach (Message mes in pendinMessages)
                        {                           
                            List<string> toDestination = mes.Destination.Split(Constants.MESSAGE_EMAIL_DEST_SEPARATOR).ToList();
                            
                            bool sendMessageStatus = true;

                            try
                            {
                                //try to sent message
                                sendMessageStatus = service.Send(mes.Subject, mes.Content, toDestination);

                            } catch(Exception ex)
                            {
                                sendMessageStatus = false;
                                mes.ErrorDescription = ex.Message;
                            }

                            if (sendMessageStatus)
                            {
                                mes.SendMessageStatus = SendMessageStatus.Sent;
                            }
                            else
                            {
                                //Increase message sent attempts
                                mes.SendingAttempts = mes.SendingAttempts + 1;

                                if (mes.SendingAttempts>= type.MaxSendingAttentps)
                                {
                                    mes.SendMessageStatus = SendMessageStatus.Error;
                                } else
                                {
                                    mes.SendMessageStatus = SendMessageStatus.Pending;
                                }
                            }

                            mes.UpdatedAt = dateInit;
                            mes.UpdatedBy = "system";
                            mes.UpdatedById = 0;

                            _context.Entry(mes).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                            _context.SaveChanges();

                        }
                    } else
                    {

                    }
                }
            } catch(Exception ex)
            {
                //put here de log error from execution
            }
        }
    }
}
