﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DGSoft.Gefinpes.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    PostalCode = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: false),
                    StreetType = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    StreetName = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: false),
                    Number = table.Column<int>(nullable: false),
                    Complement = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Owner = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    OwnerId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Labels",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    Key = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: false),
                    Description = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Labels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Languages",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: false),
                    ImageIcon = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Default = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Languages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Menus",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: false),
                    Description = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: false),
                    Uri = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    ImageIcon = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    ParentMenuId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Menus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Menus_Menus_ParentMenuId",
                        column: x => x.ParentMenuId,
                        principalTable: "Menus",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MessageTypes",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(type: "varchar(80)", maxLength: 80, nullable: false),
                    MaxCharContentNumber = table.Column<int>(nullable: false),
                    MaxCharSubjectNumber = table.Column<int>(nullable: true),
                    MaxSendingAttentps = table.Column<int>(nullable: false),
                    ServiceImpFullName = table.Column<string>(type: "varchar(254)", maxLength: 254, nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MessageTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Modules",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    ForAdmin = table.Column<bool>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    ImageIcon = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    ParentModuleId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Modules", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Modules_Modules_ParentModuleId",
                        column: x => x.ParentModuleId,
                        principalTable: "Modules",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "PlanPackages",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(type: "varchar(128)", maxLength: 128, nullable: false),
                    Description = table.Column<string>(type: "varchar(1024)", maxLength: 1024, nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    BaseValue = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanPackages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SignUpRequests",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    EmailAddress = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: false),
                    Password = table.Column<string>(type: "varchar(500)", maxLength: 500, nullable: false),
                    PhoneNumber = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: false),
                    IsWhatsapp = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    TradingName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Type = table.Column<int>(nullable: true),
                    IndividualRegistration = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    CityName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    EmployeeDescription = table.Column<string>(type: "varchar(500)", maxLength: 500, nullable: true),
                    SignupRequestStatus = table.Column<int>(nullable: false),
                    SingupRequestKey = table.Column<string>(type: "varchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SignUpRequests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SystemParams",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    Key = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Description = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: false),
                    Value = table.Column<string>(type: "varchar(4000)", maxLength: 4000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemParams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SystemVersionStatus",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemVersionStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LanguagesLabels",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    LanguageId = table.Column<long>(nullable: false),
                    LabelId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LanguagesLabels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LanguagesLabels_Labels_LabelId",
                        column: x => x.LabelId,
                        principalTable: "Labels",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_LanguagesLabels_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MenuFuncionalities",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Key = table.Column<string>(type: "varchar(40)", maxLength: 40, nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    MenuId = table.Column<long>(nullable: false),
                    InitialValue = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MenuFuncionalities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MenuFuncionalities_Menus_MenuId",
                        column: x => x.MenuId,
                        principalTable: "Menus",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Messages",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    Content = table.Column<string>(type: "varchar(2000)", maxLength: 2000, nullable: false),
                    Subject = table.Column<string>(type: "varchar(80)", maxLength: 80, nullable: false),
                    Destination = table.Column<string>(type: "varchar(1000)", maxLength: 1000, nullable: false),
                    SendingAttempts = table.Column<int>(nullable: false),
                    SendMessageStatus = table.Column<int>(nullable: false),
                    MessageTypeId = table.Column<long>(nullable: false),
                    ErrorDescription = table.Column<string>(type: "varchar(2000)", maxLength: 2000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Messages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Messages_MessageTypes_MessageTypeId",
                        column: x => x.MessageTypeId,
                        principalTable: "MessageTypes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Profiles",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    ModuleId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Profiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Profiles_Modules_ModuleId",
                        column: x => x.ModuleId,
                        principalTable: "Modules",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "SystemVersions",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(type: "varchar(60)", maxLength: 60, nullable: false),
                    Note = table.Column<string>(type: "varchar(4000)", maxLength: 4000, nullable: true),
                    VersionMajor = table.Column<int>(nullable: false),
                    VersionMinor = table.Column<int>(nullable: false),
                    SqlCreateVersionDatabase = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    SqlUpdateVersionDatabase = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    UrlFrontEndAddress = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    UrlBackEndAddress = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    SystemVersionStatusId = table.Column<long>(nullable: false),
                    ModuleId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemVersions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SystemVersions_Modules_ModuleId",
                        column: x => x.ModuleId,
                        principalTable: "Modules",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SystemVersions_SystemVersionStatus_SystemVersionStatusId",
                        column: x => x.SystemVersionStatusId,
                        principalTable: "SystemVersionStatus",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProfileMenuFuncionalities",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    ProfileId = table.Column<long>(nullable: false),
                    MenuFuncionalityId = table.Column<long>(nullable: false),
                    Granted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfileMenuFuncionalities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProfileMenuFuncionalities_MenuFuncionalities_MenuFuncionalityId",
                        column: x => x.MenuFuncionalityId,
                        principalTable: "MenuFuncionalities",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProfileMenuFuncionalities_Profiles_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Profiles",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProfileMenus",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    ProfileId = table.Column<long>(nullable: false),
                    MenuId = table.Column<long>(nullable: false),
                    View = table.Column<bool>(nullable: false),
                    Create = table.Column<bool>(nullable: false),
                    Edit = table.Column<bool>(nullable: false),
                    Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfileMenus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProfileMenus_Menus_MenuId",
                        column: x => x.MenuId,
                        principalTable: "Menus",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProfileMenus_Profiles_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Profiles",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Login = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    UserType = table.Column<int>(nullable: false),
                    CurrentTenantId = table.Column<long>(nullable: true),
                    Password = table.Column<string>(type: "varchar(500)", maxLength: 500, nullable: false),
                    ChangePassword = table.Column<bool>(nullable: false),
                    CurrentLanguageId = table.Column<long>(nullable: true),
                    ModuleId = table.Column<long>(nullable: false),
                    ProfileId = table.Column<long>(nullable: true),
                    ExternalCustommerUserId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Languages_CurrentLanguageId",
                        column: x => x.CurrentLanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Users_Modules_ModuleId",
                        column: x => x.ModuleId,
                        principalTable: "Modules",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Users_Profiles_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Profiles",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Tenants",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    DatabaseName = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    AdminUserId = table.Column<long>(nullable: true),
                    CurrrentSystemVersionId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tenants", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tenants_Users_AdminUserId",
                        column: x => x.AdminUserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Tenants_SystemVersions_CurrrentSystemVersionId",
                        column: x => x.CurrrentSystemVersionId,
                        principalTable: "SystemVersions",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedById = table.Column<long>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedById = table.Column<long>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    TradingName = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    IndividualRegistration = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    IdentificationNumber = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: true),
                    TotalNumberEmployees = table.Column<int>(nullable: true),
                    Occupation = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    TenantiId = table.Column<long>(nullable: true),
                    Login = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_Tenants_TenantiId",
                        column: x => x.TenantiId,
                        principalTable: "Tenants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Customers_TenantiId",
                table: "Customers",
                column: "TenantiId");

            migrationBuilder.CreateIndex(
                name: "idx_uk_label_key",
                table: "Labels",
                column: "Key",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_LanguagesLabels_LabelId",
                table: "LanguagesLabels",
                column: "LabelId");

            migrationBuilder.CreateIndex(
                name: "IX_LanguagesLabels_LanguageId",
                table: "LanguagesLabels",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_MenuFuncionalities_MenuId",
                table: "MenuFuncionalities",
                column: "MenuId");

            migrationBuilder.CreateIndex(
                name: "IX_Menus_ParentMenuId",
                table: "Menus",
                column: "ParentMenuId");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_MessageTypeId",
                table: "Messages",
                column: "MessageTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Modules_ParentModuleId",
                table: "Modules",
                column: "ParentModuleId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfileMenuFuncionalities_MenuFuncionalityId",
                table: "ProfileMenuFuncionalities",
                column: "MenuFuncionalityId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfileMenuFuncionalities_ProfileId",
                table: "ProfileMenuFuncionalities",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfileMenus_MenuId",
                table: "ProfileMenus",
                column: "MenuId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfileMenus_ProfileId",
                table: "ProfileMenus",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_ModuleId",
                table: "Profiles",
                column: "ModuleId");

            migrationBuilder.CreateIndex(
                name: "idx_uk_singup_req_key",
                table: "SignUpRequests",
                column: "SingupRequestKey",
                unique: true,
                filter: "[SingupRequestKey] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "idx_uk_system_param_key",
                table: "SystemParams",
                column: "Key",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SystemVersions_ModuleId",
                table: "SystemVersions",
                column: "ModuleId");

            migrationBuilder.CreateIndex(
                name: "IX_SystemVersions_SystemVersionStatusId",
                table: "SystemVersions",
                column: "SystemVersionStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Tenants_AdminUserId",
                table: "Tenants",
                column: "AdminUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Tenants_CurrrentSystemVersionId",
                table: "Tenants",
                column: "CurrrentSystemVersionId");

            migrationBuilder.CreateIndex(
                name: "idx_uk_tenant_databasename",
                table: "Tenants",
                column: "DatabaseName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_CurrentLanguageId",
                table: "Users",
                column: "CurrentLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_CurrentTenantId",
                table: "Users",
                column: "CurrentTenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_ModuleId",
                table: "Users",
                column: "ModuleId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_ProfileId",
                table: "Users",
                column: "ProfileId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Tenants_CurrentTenantId",
                table: "Users",
                column: "CurrentTenantId",
                principalTable: "Tenants",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Tenants_CurrentTenantId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "LanguagesLabels");

            migrationBuilder.DropTable(
                name: "Messages");

            migrationBuilder.DropTable(
                name: "PlanPackages");

            migrationBuilder.DropTable(
                name: "ProfileMenuFuncionalities");

            migrationBuilder.DropTable(
                name: "ProfileMenus");

            migrationBuilder.DropTable(
                name: "SignUpRequests");

            migrationBuilder.DropTable(
                name: "SystemParams");

            migrationBuilder.DropTable(
                name: "Labels");

            migrationBuilder.DropTable(
                name: "MessageTypes");

            migrationBuilder.DropTable(
                name: "MenuFuncionalities");

            migrationBuilder.DropTable(
                name: "Menus");

            migrationBuilder.DropTable(
                name: "Tenants");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "SystemVersions");

            migrationBuilder.DropTable(
                name: "Languages");

            migrationBuilder.DropTable(
                name: "Profiles");

            migrationBuilder.DropTable(
                name: "SystemVersionStatus");

            migrationBuilder.DropTable(
                name: "Modules");
        }
    }
}
