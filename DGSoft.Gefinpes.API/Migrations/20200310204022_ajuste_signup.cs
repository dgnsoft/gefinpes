﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DGSoft.Gefinpes.Migrations
{
    public partial class ajuste_signup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Login",
                table: "Customers");

            migrationBuilder.AddColumn<long>(
                name: "TenantId",
                table: "SignUpRequests",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SignUpRequests_TenantId",
                table: "SignUpRequests",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_SignUpRequests_Tenants_TenantId",
                table: "SignUpRequests",
                column: "TenantId",
                principalTable: "Tenants",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SignUpRequests_Tenants_TenantId",
                table: "SignUpRequests");

            migrationBuilder.DropIndex(
                name: "IX_SignUpRequests_TenantId",
                table: "SignUpRequests");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "SignUpRequests");

            migrationBuilder.AddColumn<string>(
                name: "Login",
                table: "Customers",
                type: "varchar(120)",
                maxLength: 120,
                nullable: false,
                defaultValue: "");
        }
    }
}
