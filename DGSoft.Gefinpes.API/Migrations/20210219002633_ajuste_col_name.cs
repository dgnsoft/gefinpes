﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DGSoft.Gefinpes.Migrations
{
    public partial class ajuste_col_name : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Tenants",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "SystemVersionStatus",
                type: "varchar(100)",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(60)",
                oldMaxLength: 60);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "SystemVersionStatus",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "SystemVersionStatus",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "SystemVersionStatus",
                type: "varchar(120)",
                maxLength: 120,
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedById",
                table: "SystemVersionStatus",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "SystemVersionStatus",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy",
                table: "SystemVersionStatus",
                type: "varchar(120)",
                maxLength: 120,
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UpdatedById",
                table: "SystemVersionStatus",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "SystemVersions",
                type: "varchar(100)",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(60)",
                oldMaxLength: 60);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "SystemVersions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "SystemParams",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "SystemParams",
                type: "varchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "SignUpRequests",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "ProfileMenus",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "ProfileMenus",
                type: "varchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "ProfileMenuFuncionalities",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "ProfileMenuFuncionalities",
                type: "varchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "PlanPackages",
                type: "varchar(100)",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(128)",
                oldMaxLength: 128);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "MessageTypes",
                type: "varchar(100)",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(80)",
                oldMaxLength: 80);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Messages",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Messages",
                type: "varchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Menus",
                type: "varchar(100)",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(60)",
                oldMaxLength: 60);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Menus",
                type: "varchar(60)",
                maxLength: 60,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(120)",
                oldMaxLength: 120);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "MenuFuncionalities",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "MenuFuncionalities",
                type: "varchar(120)",
                maxLength: 120,
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CreatedById",
                table: "MenuFuncionalities",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "MenuFuncionalities",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy",
                table: "MenuFuncionalities",
                type: "varchar(120)",
                maxLength: 120,
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UpdatedById",
                table: "MenuFuncionalities",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "LanguagesLabels",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "LanguagesLabels",
                type: "varchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Languages",
                type: "varchar(100)",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(60)",
                oldMaxLength: 60);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Languages",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Labels",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Labels",
                type: "varchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Addresses",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Addresses",
                type: "varchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "Tenants");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "SystemVersionStatus");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "SystemVersionStatus");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "SystemVersionStatus");

            migrationBuilder.DropColumn(
                name: "CreatedById",
                table: "SystemVersionStatus");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "SystemVersionStatus");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "SystemVersionStatus");

            migrationBuilder.DropColumn(
                name: "UpdatedById",
                table: "SystemVersionStatus");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "SystemVersions");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "SystemParams");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "SystemParams");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "SignUpRequests");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "ProfileMenus");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "ProfileMenus");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "ProfileMenuFuncionalities");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "ProfileMenuFuncionalities");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "MenuFuncionalities");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "MenuFuncionalities");

            migrationBuilder.DropColumn(
                name: "CreatedById",
                table: "MenuFuncionalities");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "MenuFuncionalities");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "MenuFuncionalities");

            migrationBuilder.DropColumn(
                name: "UpdatedById",
                table: "MenuFuncionalities");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "LanguagesLabels");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "LanguagesLabels");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "Languages");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "Labels");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Labels");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "Addresses");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Addresses");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "SystemVersionStatus",
                type: "varchar(60)",
                maxLength: 60,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(100)",
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "SystemVersions",
                type: "varchar(60)",
                maxLength: 60,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(100)",
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "PlanPackages",
                type: "varchar(128)",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(100)",
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "MessageTypes",
                type: "varchar(80)",
                maxLength: 80,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(100)",
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Menus",
                type: "varchar(60)",
                maxLength: 60,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(100)",
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Menus",
                type: "varchar(120)",
                maxLength: 120,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(60)",
                oldMaxLength: 60);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Languages",
                type: "varchar(60)",
                maxLength: 60,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(100)",
                oldMaxLength: 100);
        }
    }
}
