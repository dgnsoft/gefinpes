IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [Addresses] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedById] bigint NULL,
    [CreatedBy] varchar(120) NULL,
    [CreatedAt] datetime2 NULL,
    [UpdatedById] bigint NULL,
    [UpdatedBy] varchar(120) NULL,
    [UpdatedAt] datetime2 NULL,
    [PostalCode] varchar(20) NOT NULL,
    [StreetType] varchar(100) NULL,
    [StreetName] varchar(150) NOT NULL,
    [Number] int NOT NULL,
    [Complement] varchar(100) NULL,
    [Owner] varchar(100) NOT NULL,
    [OwnerId] bigint NOT NULL,
    CONSTRAINT [PK_Addresses] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Labels] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedById] bigint NULL,
    [CreatedBy] varchar(120) NULL,
    [CreatedAt] datetime2 NULL,
    [UpdatedById] bigint NULL,
    [UpdatedBy] varchar(120) NULL,
    [UpdatedAt] datetime2 NULL,
    [Key] varchar(60) NOT NULL,
    [Description] varchar(100) NULL,
    CONSTRAINT [PK_Labels] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Languages] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedById] bigint NULL,
    [CreatedBy] varchar(120) NULL,
    [CreatedAt] datetime2 NULL,
    [UpdatedById] bigint NULL,
    [UpdatedBy] varchar(120) NULL,
    [UpdatedAt] datetime2 NULL,
    [Name] varchar(60) NOT NULL,
    [ImageIcon] varchar(100) NULL,
    [Default] bit NOT NULL,
    CONSTRAINT [PK_Languages] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Menus] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedById] bigint NULL,
    [CreatedBy] varchar(120) NULL,
    [CreatedAt] datetime2 NULL,
    [UpdatedById] bigint NULL,
    [UpdatedBy] varchar(120) NULL,
    [UpdatedAt] datetime2 NULL,
    [Name] varchar(60) NOT NULL,
    [Description] varchar(120) NOT NULL,
    [Uri] varchar(255) NULL,
    [ImageIcon] varchar(100) NULL,
    [Active] bit NOT NULL,
    [ParentMenuId] bigint NULL,
    CONSTRAINT [PK_Menus] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Menus_Menus_ParentMenuId] FOREIGN KEY ([ParentMenuId]) REFERENCES [Menus] ([Id])
);

GO

CREATE TABLE [Modules] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedById] bigint NULL,
    [CreatedBy] varchar(120) NULL,
    [CreatedAt] datetime2 NULL,
    [UpdatedById] bigint NULL,
    [UpdatedBy] varchar(120) NULL,
    [UpdatedAt] datetime2 NULL,
    [Name] varchar(100) NOT NULL,
    [ForAdmin] bit NOT NULL,
    [Active] bit NOT NULL,
    [ImageIcon] varchar(100) NULL,
    [ParentModuleId] bigint NULL,
    CONSTRAINT [PK_Modules] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Modules_Modules_ParentModuleId] FOREIGN KEY ([ParentModuleId]) REFERENCES [Modules] ([Id])
);

GO

CREATE TABLE [SystemParams] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedById] bigint NULL,
    [CreatedBy] varchar(120) NULL,
    [CreatedAt] datetime2 NULL,
    [UpdatedById] bigint NULL,
    [UpdatedBy] varchar(120) NULL,
    [UpdatedAt] datetime2 NULL,
    [Key] varchar(60) NOT NULL,
    [Type] int NOT NULL,
    [Description] varchar(256) NOT NULL,
    [Value] varchar(4000) NULL,
    CONSTRAINT [PK_SystemParams] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [LanguagesLabels] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedById] bigint NULL,
    [CreatedBy] varchar(120) NULL,
    [CreatedAt] datetime2 NULL,
    [UpdatedById] bigint NULL,
    [UpdatedBy] varchar(120) NULL,
    [UpdatedAt] datetime2 NULL,
    [LanguageId] bigint NOT NULL,
    [LabelId] bigint NOT NULL,
    CONSTRAINT [PK_LanguagesLabels] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_LanguagesLabels_Labels_LabelId] FOREIGN KEY ([LabelId]) REFERENCES [Labels] ([Id]),
    CONSTRAINT [FK_LanguagesLabels_Languages_LanguageId] FOREIGN KEY ([LanguageId]) REFERENCES [Languages] ([Id])
);

GO

CREATE TABLE [MenuFuncionalities] (
    [Id] bigint NOT NULL IDENTITY,
    [Name] varchar(100) NOT NULL,
    [Key] varchar(40) NOT NULL,
    [Active] bit NOT NULL,
    [MenuId] bigint NOT NULL,
    [InitialValue] bit NOT NULL,
    CONSTRAINT [PK_MenuFuncionalities] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_MenuFuncionalities_Menus_MenuId] FOREIGN KEY ([MenuId]) REFERENCES [Menus] ([Id])
);

GO

CREATE TABLE [Profiles] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedById] bigint NULL,
    [CreatedBy] varchar(120) NULL,
    [CreatedAt] datetime2 NULL,
    [UpdatedById] bigint NULL,
    [UpdatedBy] varchar(120) NULL,
    [UpdatedAt] datetime2 NULL,
    [Name] varchar(100) NOT NULL,
    [Active] bit NOT NULL,
    [ModuleId] bigint NULL,
    CONSTRAINT [PK_Profiles] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Profiles_Modules_ModuleId] FOREIGN KEY ([ModuleId]) REFERENCES [Modules] ([Id])
);

GO

CREATE TABLE [ProfileMenuFuncionalities] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedById] bigint NULL,
    [CreatedBy] varchar(120) NULL,
    [CreatedAt] datetime2 NULL,
    [UpdatedById] bigint NULL,
    [UpdatedBy] varchar(120) NULL,
    [UpdatedAt] datetime2 NULL,
    [ProfileId] bigint NOT NULL,
    [MenuFuncionalityId] bigint NOT NULL,
    [Granted] bit NOT NULL,
    CONSTRAINT [PK_ProfileMenuFuncionalities] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ProfileMenuFuncionalities_MenuFuncionalities_MenuFuncionalityId] FOREIGN KEY ([MenuFuncionalityId]) REFERENCES [MenuFuncionalities] ([Id]),
    CONSTRAINT [FK_ProfileMenuFuncionalities_Profiles_ProfileId] FOREIGN KEY ([ProfileId]) REFERENCES [Profiles] ([Id])
);

GO

CREATE TABLE [ProfileMenus] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedById] bigint NULL,
    [CreatedBy] varchar(120) NULL,
    [CreatedAt] datetime2 NULL,
    [UpdatedById] bigint NULL,
    [UpdatedBy] varchar(120) NULL,
    [UpdatedAt] datetime2 NULL,
    [ProfileId] bigint NOT NULL,
    [MenuId] bigint NOT NULL,
    [View] bit NOT NULL,
    [Create] bit NOT NULL,
    [Edit] bit NOT NULL,
    [Delete] bit NOT NULL,
    CONSTRAINT [PK_ProfileMenus] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ProfileMenus_Menus_MenuId] FOREIGN KEY ([MenuId]) REFERENCES [Menus] ([Id]),
    CONSTRAINT [FK_ProfileMenus_Profiles_ProfileId] FOREIGN KEY ([ProfileId]) REFERENCES [Profiles] ([Id])
);

GO

CREATE TABLE [Users] (
    [Id] bigint NOT NULL IDENTITY,
    [CreatedById] bigint NULL,
    [CreatedBy] varchar(120) NULL,
    [CreatedAt] datetime2 NULL,
    [UpdatedById] bigint NULL,
    [UpdatedBy] varchar(120) NULL,
    [UpdatedAt] datetime2 NULL,
    [Name] varchar(100) NOT NULL,
    [Login] varchar(120) NOT NULL,
    [Active] bit NOT NULL,
    [UserType] int NOT NULL,
    [CurrentTenantId] bigint NOT NULL,
    [Password] varchar(500) NOT NULL,
    [ChangePassword] bit NOT NULL,
    [ProfileId] bigint NULL,
    [ExternalAdminUserId] bigint NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Users_Profiles_ProfileId] FOREIGN KEY ([ProfileId]) REFERENCES [Profiles] ([Id])
);

GO

CREATE UNIQUE INDEX [idx_uk_label_key] ON [Labels] ([Key]);

GO

CREATE INDEX [IX_LanguagesLabels_LabelId] ON [LanguagesLabels] ([LabelId]);

GO

CREATE INDEX [IX_LanguagesLabels_LanguageId] ON [LanguagesLabels] ([LanguageId]);

GO

CREATE INDEX [IX_MenuFuncionalities_MenuId] ON [MenuFuncionalities] ([MenuId]);

GO

CREATE INDEX [IX_Menus_ParentMenuId] ON [Menus] ([ParentMenuId]);

GO

CREATE INDEX [IX_Modules_ParentModuleId] ON [Modules] ([ParentModuleId]);

GO

CREATE INDEX [IX_ProfileMenuFuncionalities_MenuFuncionalityId] ON [ProfileMenuFuncionalities] ([MenuFuncionalityId]);

GO

CREATE INDEX [IX_ProfileMenuFuncionalities_ProfileId] ON [ProfileMenuFuncionalities] ([ProfileId]);

GO

CREATE INDEX [IX_ProfileMenus_MenuId] ON [ProfileMenus] ([MenuId]);

GO

CREATE INDEX [IX_ProfileMenus_ProfileId] ON [ProfileMenus] ([ProfileId]);

GO

CREATE INDEX [IX_Profiles_ModuleId] ON [Profiles] ([ModuleId]);

GO

CREATE UNIQUE INDEX [idx_uk_system_param_key] ON [SystemParams] ([Key]);

GO

CREATE INDEX [IX_Users_ProfileId] ON [Users] ([ProfileId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200306181255_inicial', N'3.1.1');

GO




