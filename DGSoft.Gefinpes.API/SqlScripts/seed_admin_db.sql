set identity_insert Modules on;

insert into Modules(id, Name, ForAdmin, Active, ImageIcon) values (1,'Product Admin',1,0,'settings');
insert into Modules(id, Name, ForAdmin, Active, ImageIcon) values (2,'Customer',0,0,'settings');

set identity_insert Modules off;

set identity_insert Profiles on;

insert into Profiles(id, Name, Active,ModuleId, CreatedById, CreatedBy, CreatedAt, UpdatedById, UpdatedBy, UpdatedAt)
values (1,'Master Admin', 1,1, 0,'admin',GETDATE(), 0, 'admin', GETDATE());

set identity_insert Profiles off;

set identity_insert Users on;

insert into Users(id, Name, Login, Active,UserType, ModuleId, Password, ChangePassword, ProfileId)
values (1,'Admin', 'admin', 1,1,1,'2bfu7lY2rcADv/7vFfMVqpaojoyfETIi1wxE0mTlKPzQva5PZD0lD+vxFAKoeR0hU/u90ExaqvcgvU7frPsEvCTz/yWbpH4DAcvDeOEVOARMIO9Z4/WLG5S5huDbPwWe',0,1);

set identity_insert Users off;

set identity_insert SystemVersionStatus on;
insert into SystemVersionStatus(id, Name) values (1,'Production');
insert into SystemVersionStatus(id, Name) values (2,'Test');
insert into SystemVersionStatus(id, Name) values (3,'Homologation');
set identity_insert SystemVersionStatus off;

set identity_insert SystemVersions on;

insert into SystemVersions(id, Name, Note,VersionMajor, VersionMinor, SqlCreateVersionDatabase, SystemVersionStatusId, ModuleId)
values (0,'Admin Version', 'Version for Admin Product',1,0,'', 1,1);

insert into SystemVersions(id, Name, Note,VersionMajor, VersionMinor, SqlCreateVersionDatabase,SqlUpdateVersionDatabase, SystemVersionStatusId, ModuleId)
values (1,'Custommer Version', 'Version for Custommer Product',1,0,'create_database_v01.sql', 'seed_custommer_db_v1.sql',1,2);

set identity_insert SystemVersions off;

set identity_insert Tenants on;

insert into Tenants(id, Name, DatabaseName, AdminUserId, CurrrentSystemVersionId)
values (1,'Admin Product', 'productadmin_db',1,0);

set identity_insert Tenants off;

update Users set CurrentTenantId=1 where id=1;

set identity_insert Menus on;

insert into Menus(id, Name, Description, Uri, ImageIcon, Active, ParentMenuId)
values (1,'Configuration', 'Configurações',null,'fa-group',1,null);

insert into Menus(id, Name, Description, Uri, ImageIcon, Active, ParentMenuId)
values (2,'Profiles', 'Cadastro de perfis','/config/profiles','fa-group',1,1);

set identity_insert Menus off;

set identity_insert MessageTypes on;

insert into MessageTypes(id, Name, MaxCharSubjectNumber, MaxCharContentNumber, MaxSendingAttentps,  Active, ServiceImpFullName)
values (1,'Email', 80, 2000, 3, 1,'Domain.BLL.EmailService');

set identity_insert MessageTypes off;

insert into ProfileMenus(ProfileId, MenuId, [View],[Create], [Delete], [Edit], CreatedBy, CreatedById, CreatedAt)
select 1,m.id,1,1,1,1,'admin',null,GETDATE() from Menus m where ParentMenuId is not null
and not exists(select 1 from ProfileMenus pm where pm.ProfileId = 1 and pm.MenuId = m.Id);
