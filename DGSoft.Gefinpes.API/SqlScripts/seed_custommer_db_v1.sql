set identity_insert Profiles on;

insert into Profiles(id, Name, Active, CreatedById, CreatedBy, CreatedAt, UpdatedById, UpdatedBy, UpdatedAt)
values (1,'Administrador', 1, 0,'admin',GETDATE(), 0, 'admin', GETDATE());

set identity_insert Profiles off;

set identity_insert Users on;

insert into Users(id, Name, Login, Active,UserType, Password, ChangePassword, ProfileId, CurrentTenantId, ExternalAdminUserId)
values (1,@userName, @userLogin, 1,1,@userPassword,0,1, 2, @externalAdminUserId);

set identity_insert Users off;

set identity_insert Menus on;

insert into Menus(id, Name, Description, Uri, ImageIcon, Active, ParentMenuId)
values (1,'Configuration', 'Configurações',null,'fa-group',1,null);

insert into Menus(id, Name, Description, Uri, ImageIcon, Active, ParentMenuId)
values (2,'Profiles', 'Cadastro de perfis','/config/profiles','fa-group',1,1);

insert into Menus(id, Name, Description, Uri, ImageIcon, Active, ParentMenuId)
values (3,'Users', 'Cadastro de usuários','/config/users','fa-user',1,1);

set identity_insert Menus off;

insert into ProfileMenus(ProfileId, MenuId, [View],[Create], [Delete], [Edit], CreatedBy, CreatedById, CreatedAt)
select 1,m.id,1,1,1,1,'admin',null,GETDATE() from Menus m where ParentMenuId is not null
and not exists(select 1 from ProfileMenus pm where pm.ProfileId = 1 and pm.MenuId = m.Id);