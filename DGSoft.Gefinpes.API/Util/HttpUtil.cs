﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DGSoft.Gefinpes.API.Util
{
    /// <summary>
    /// 
    /// </summary>
    public static class HttpUtil
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ModelState"></param>
        /// <returns></returns>
        public static List<String> GetErrorsFromModelstate(ModelStateDictionary ModelState)
        {
            List<string> errors = new List<string>();
            if (!ModelState.IsValid)
            {
                errors = ModelState.Values
                            .SelectMany(x => x.Errors)
                            .Select(x => x.ErrorMessage).ToList();
            }
            else
            {

            }
            return errors;
        }
    }
}
