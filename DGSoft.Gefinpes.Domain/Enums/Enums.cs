﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Core
{
    public static class Enums
    {
        public static int Descricao { get; set; }

        public enum Modules
        {
            ProductAdmin=1,
            Custommer=2
        }

        public enum SystemParamType
        {
            Integer = 1,
            Text= 2,
            Boolean = 3,
            ListOfInteger = 4,
            ListOfString=5
        }

        public enum SendMessageStatus
        {
            Pending = 0,
            InProcess = 1,
            Sent = 2,
            Error= 3
        }

        public enum MessageTypes
        {
            Email = 1
        }

        public enum SignupRequestStatus
        {
            Pending = 0,
            InProcess = 1,
            Processed = 2,
            Canceled =3,
            Error=4
        }

        public enum SystemVersionStatus
        {
            Production = 1,
            Test = 2,
            Homologation = 3
        }
    }
}
