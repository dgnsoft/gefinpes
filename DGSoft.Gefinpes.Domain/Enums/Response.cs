﻿using Amazon.ElasticBeanstalk.Model.Internal.MarshallTransformations;
using System;
using System.Collections.Generic;
using System.Text;

namespace DGSoft.Gefinpes.Domain.Enums
{
    public static class Response
    {
        public enum ExceptionLevel
        {
            InternalError,
            NoDataFound,
            ValidationError,
            None
        }

        public enum ExceptionType
        {
            Alert,
            Error,            
            None
        }
    }
}
