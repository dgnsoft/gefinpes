﻿using System;

namespace Domain.Core.Exceptions
{
    public class DALException : Exception
    {
        public DALException()
        {
            // Add implementation.
        }
        public DALException(string message) : base(message)
        {
            // Add implementation.
        }
        public DALException(string message, Exception inner) : base(message, inner)
        {
            // Add implementation.
        }
     }
}
