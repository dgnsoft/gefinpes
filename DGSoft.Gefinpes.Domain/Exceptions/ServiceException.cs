﻿using System;

namespace Domain.Core.Exceptions
{
    public class ServiceException : Exception
    {
        public ServiceException()
        {
            // Add implementation.
        }
        public ServiceException(string message) : base(message)
        {
            // Add implementation.
        }
        public ServiceException(string message, Exception inner) : base(message, inner)
        {
            // Add implementation.
        }
     }
}
