﻿using System;

namespace Domain.Core.Exceptions
{
    public class ValidationErrorException : Exception
    {
        public ValidationErrorException()
        {
            // Add implementation.
        }
        public ValidationErrorException(string message) : base(message)
        {
            // Add implementation.
        }
        public ValidationErrorException(string message, Exception inner) : base(message, inner)
        {
            // Add implementation.
        }
     }
}
