﻿using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Domain.DAL.Repository.Interfaces
{
    public interface IGenericRepository<T> where T  : EntityBase
    {
        T Create(T entity, UserDTO _userLoggedData = null);
        void Delete(T entity, UserDTO _userLoggedData = null);
        void DeleteById(long id, UserDTO _userLoggedData = null);
        T Edit(long id, T entity, UserDTO _userLoggedData = null);
        T GetById(long id);
        IEnumerable<T> Filter();
        IEnumerable<T> Filter(Expression<Func<T, bool>> expression);        
    }
}
