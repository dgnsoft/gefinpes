﻿using Domain.Models;
using Domain.Models.DTO;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace Domain.BLL.Intefaces
{
    public interface IAuthService
    {
        public User GetUserByLogin(string login, long ModuleId);

        UserDTO Authenticate(string login, string password);

        UserCustommerDTO GetUserCustommerDataByLogin(string login);

        UserCustommerFullDTO AutenticateUserCustommer(string login, string password);

        string GenerateToken(UserDTO user);

        string GetLoggedUserName(ClaimsPrincipal identity);

        string GetLoggedUserDatabaseName(ClaimsPrincipal identity);

        long GetLoggedUserId(ClaimsPrincipal identity);

        UserDTO GetLoggedUserData(ClaimsPrincipal identity);

        ProfileMenu GetProfileMenuPermission(string DatabaseName, long ProfileId, long MenuId);

        ProfileMenuFuncionality GetProfilemMenuFunctionalityPermission(string DataBaseName, long ProfileId, long MenuFunctionalityId);
        bool CheckUserPassword(User us, string Password);

        List<Menu> GetMenusForProfileLoggedUser(long ProfileId);

    }
}
