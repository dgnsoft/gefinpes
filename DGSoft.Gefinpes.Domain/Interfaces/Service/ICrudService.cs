﻿using Domain.Models.DTO;
using Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.BLL.Interfaces
{
    public interface ICrudService<TEntity, TSearchDTO> where TEntity : IEntity where TSearchDTO : class
    {
        
        void Create(TEntity entity);
        void Delete(TEntity entity);
        void DeleteById(long id);
        void Edit(long id, TEntity entity);

        void SetUserLoggedData(UserDTO userDTO);
        TEntity GetById(long id);
        IEnumerable<TEntity> Filter();
        IEnumerable<TEntity> Filter(Func<TEntity, bool> predicate);
        IEnumerable<TEntity> Search(TSearchDTO searchDTO);
    }
}
