﻿using Domain.Core;
using Domain.Models;
using Domain.Models.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.BLL.Interfaces
{
    public interface IDBService
    {
        bool IsDatabaseCreated(string dbName);
        bool IsDatabaseLastVersion(string dbName);
        bool MigrateToLastVersion(string dbName);
        bool ExecuteTenantCreationDb(Tenant tenant);
        bool SeedInitialCustommerData(string dbName, string SeedInitialDataScript, User user);
        bool ExecuteScript(string scriptName, string dbName);
        bool CreateDatabaseByScript(string dbName);
        SystemVersion GetLastCurrentSystemVersion(long ModuleId, Enums.SystemVersionStatus Status);
    }
}
