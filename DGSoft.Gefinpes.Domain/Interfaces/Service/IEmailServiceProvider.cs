﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.BLL.Interfaces
{
    public interface IEmailServiceProvider
    {
        bool SendEmail(string subject, string content, List<string> toAddresses);
    }
}
