﻿using DGSoft.Gefinpes.Domain.Models.Core;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using static DGSoft.Gefinpes.Domain.Enums.Response;

namespace Domain.BLL.Interfaces
{
    public interface IGenericService<T> where T : EntityBase
    {
        
        ResponseModel<T> Create(T entity);
        ResponseModel<T> Delete(T entity);
        ResponseModel<T> DeleteById(long id);
        ResponseModel<T> Edit(long id, T entity);
        ResponseModel<T> GetById(long id);
        ResponseModel<T> Filter();
        ResponseModel<T> Filter(Expression<Func<T, bool>> expression);
    }
}
