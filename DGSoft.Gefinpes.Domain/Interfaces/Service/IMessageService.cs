﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.BLL.Interfaces
{
    public interface IMessageService
    {
        bool Send(string subject, string content, List<string> toAddresses);
        Domain.Models.Message CreateNewMessage(string subject, string content, List<string> toAddresses, DateTime dtAction, string userCreated, long? userCreatedId = 0 );
    }
}
