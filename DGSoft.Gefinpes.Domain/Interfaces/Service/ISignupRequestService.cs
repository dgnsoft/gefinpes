﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.BLL.Interfaces
{
    public interface ISignupRequestService
    {
        SignupRequest Create(SignupRequest signup);

        bool ConfirmSignup(string signupKey);
    }
}
