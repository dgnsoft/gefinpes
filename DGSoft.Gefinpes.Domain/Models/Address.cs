﻿
using Domain.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.Models
{
    [Table("Addresses")]
    public class Address : EntityBase
    {

       [Required, MaxLength(20)]
       public string PostalCode { get; set; }
        [MaxLength(100)]
        public string StreetType { get; set; }
       
       [Required, MaxLength(150)]
       public string StreetName { get; set; }
        [Required]
      public int Number { get; set; }
        [MaxLength(100)]
       public string Complement { get; set; }

        [Required, MaxLength(100)]
        public string Owner { get; set; }
        [Required]
        public long OwnerId { get; set; }
    }
}
