﻿using System;
using System.Collections.Generic;
using System.Text;
using static DGSoft.Gefinpes.Domain.Enums.Response;

namespace DGSoft.Gefinpes.Domain.Models.Core
{
    public class ResponseModel<T>
    {
        public List<string> Message { get; set; }
        public List<string> Errors { get; set; }
        public List<T> Result { get; set; }
        public bool Success { get; set; }
        public ExceptionLevel ExceptionLevel { get; set; }
        public ExceptionType ExceptionType { get; set; }

        public ResponseModel(){
            this.Message = new List<string>();
            this.Result = new List<T>();
            this.Errors = new List<string>();
            this.Success = true;
        }
    }
}
