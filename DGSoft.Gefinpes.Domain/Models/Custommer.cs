﻿using Domain.Models.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.Models
{
    [Table("Customers")]
    public class Customer : EntityBase, IEntity
    {
              
        [Required]
        public int Type { get; set; } //1 - PF ou 2 - PJ

        [MaxLength(255)]
        public string TradingName { get; set; }
        
        [MaxLength(50)]
        public string IndividualRegistration { get; set; } //CPF ou CNPJ
        [MaxLength(100)]
        public string IdentificationNumber { get; set; } //RG ou IE
        
        public DateTime? BirthDate { get; set; }
        
        public int? TotalNumberEmployees { get; set; }

        [MaxLength(255)]
        public string Occupation { get; set; } 

        [ForeignKey("Tenant")]
        public long? TenantiId { get; set; }
        public virtual Tenant Tenant { get; set; }

    }
}
