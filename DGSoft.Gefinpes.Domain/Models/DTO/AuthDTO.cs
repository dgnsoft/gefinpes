﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class AuthDTO
    {
        public string Login { get; set; }
        public string Password { get; set; }

    }
}
