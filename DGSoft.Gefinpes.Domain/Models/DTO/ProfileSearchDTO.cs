﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class ProfileSearchDTO
    {
        public string Name { get; set; }
        public bool? Active { get; set; }
    }
}
