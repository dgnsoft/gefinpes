﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class UserCustommerDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public long ProfileId { get; set; }
        public string ProfileName { get; set; }
        public bool Active { get; set; }
        public long SystemVersionId { get; set; }
        public string SystemVersionNumber { get; set; }
        public string UrlFrontEndAddress { get; set; }
        public string UrlBackEndAddress { get; set; }

    }
}
