﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class UserCustommerFullDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public string Login { get; set; }
        public long? CurrentTenantId { get; set; }
        public string TenantName { get; set; }
        public string DatabaseName { get; set; }
        public long ModuleId { get; set; }
        public string ModuleName { get; set; }
        public long ProfileId { get; set; }
        public string ProfileName { get; set; }
        public DateTime AuthenticationDate { get; set; }
        
        public long SystemVersionId { get; set; }
        public string SystemVersionNumber { get; set; }
        public string UrlFrontEndAddress { get; set; }
        public string UrlBackEndAddress { get; set; }

    }
}
