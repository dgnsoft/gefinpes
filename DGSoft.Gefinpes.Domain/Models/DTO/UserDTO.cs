﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class UserDTO : EntityBase
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public int UserType { get; set; } //1= administrator / 2 - user
        public long? CurrentTenantId { get; set; }
        public string TenantName { get; set; }
        public string DatabaseName { get; set; }
        public long ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string access_token { get; set; }
        public long ProfileId { get; set; }
        public string ProfileName { get; set; }
        public DateTime LoginDate { get; set; }
    }
}
