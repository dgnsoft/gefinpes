﻿using Domain.Models.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    public abstract class EntityBase : IEntityBase
    {
        [Key]
        public long Id { get; set; }

        [MaxLength(100), Required]
        public string Name { get; set; }

        [Column("Active")]
        public bool Active { get; set; }
        public long? CreatedById { get; set; }
        [MaxLength(120)]
        public string CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public long? UpdatedById { get; set; }
        [MaxLength(120)]
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
