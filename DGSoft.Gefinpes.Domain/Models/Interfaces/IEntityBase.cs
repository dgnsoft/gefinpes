﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Models.Interfaces
{
    public interface IEntityBase
    {
        long Id { get; set; }
        bool Active { get; set; }
        long? CreatedById { get; set; }
        string CreatedBy { get; set; }
        DateTime? CreatedAt { get; set; }
        long? UpdatedById { get; set; }
        string UpdatedBy { get; set; }
        DateTime? UpdatedAt { get; set; }
    }
}
