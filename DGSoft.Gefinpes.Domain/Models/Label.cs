﻿
using Domain.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.Models
{
    [Table("Labels")]
    public class Label : EntityBase
    {       

        [MaxLength(60),Required]        
        public string Key { get; set; }

        [MaxLength(100)]
        public string Description { get; set; }
                
    }
}
