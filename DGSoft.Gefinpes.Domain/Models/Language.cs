﻿using Domain.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.Models
{
    [Table("Languages")]
    public class Language : EntityBase
    {
      
        [MaxLength(100)]
        public string ImageIcon { get; set; }    
        [Required]
        public bool Default { get; set; }
    }
}
