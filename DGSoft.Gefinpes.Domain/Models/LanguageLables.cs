﻿using Domain.Models;
using Domain.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.Models
{
    [Table("LanguagesLabels")]
    public class LanguageLabels : EntityBase
    {
        [ForeignKey("Language")]
        public long LanguageId { get; set; }
        public virtual Language Language { get; set; }

        [ForeignKey("Label")]
        public long LabelId { get; set; }
        public virtual Label Label { get; set; }
    }
}
