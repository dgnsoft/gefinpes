﻿using Domain.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.Models
{
    [Table("Menus")]
    public class Menu : EntityBase
    {        

        [MaxLength(60),Required]
        public string Description { get; set; }
        [MaxLength(255)]
        public string Uri { get; set; }
        [MaxLength(100)]
        public string ImageIcon { get; set; }

        [ForeignKey("ParentMenu"), ]
        public long? ParentMenuId { get; set; }
        public virtual Menu ParentMenu { get; set; }


    }
}
