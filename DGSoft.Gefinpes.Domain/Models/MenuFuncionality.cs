﻿using Domain.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.Models
{
    [Table("MenuFuncionalities")]
    public class MenuFuncionality : EntityBase
    {

        [MaxLength(40), Required] //TODO Make index to be unique
        public string Key { get; set; }
    
        [ForeignKey("Menu")]
        public long MenuId { get; set; }
        public virtual Menu Menu { get; set; }

        [Required]
        public bool InitialValue { get; set; }

    }
}
