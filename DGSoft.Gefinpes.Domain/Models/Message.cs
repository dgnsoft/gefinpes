﻿using Domain.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static Domain.Core.Enums;

namespace Domain.Models
{
    [Table("Messages")]
    public class Message : EntityBase, IEntity
    {
        public Message()
        {
            this.SendMessageStatus = SendMessageStatus.Pending;
        }

        public Message(long _MessageTypeId)
        {
            this.MessageTypeId = _MessageTypeId;
            this.SendMessageStatus = SendMessageStatus.Pending;
        }

        [MaxLength(2000), Required]
        public string Content { get; set; }

        [MaxLength(80), Required]
        public string Subject { get; set; }

        [MaxLength(1000), Required]
        public string Destination { get; set; }
        
        public int SendingAttempts { get; set; } //tentativas de envio

        [Required]
        public SendMessageStatus SendMessageStatus{ get; set; }

        [Required, ForeignKey("MessageType")]
        public long MessageTypeId { get; set; }
        public virtual MessageType MessageType { get; set; }

        [MaxLength(2000)]
        public string ErrorDescription { get; set; }
    }
}
