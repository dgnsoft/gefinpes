﻿using Domain.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    [Table("MessageTypes")]
    public class MessageType : EntityBase, IEntity
    {
     
        public int MaxCharContentNumber { get; set; }

        public int? MaxCharSubjectNumber { get; set; }

        public int MaxSendingAttentps { get; set; }

        [MaxLength(254),Required]
        public string ServiceImpFullName { get; set; }

    }
}
