﻿using Domain.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.Models
{
    [Table("Modules")]
    public class Module : EntityBase
    {       
        public bool ForAdmin { get; set; }
        [MaxLength(100)]
        public string ImageIcon { get; set; }

        [ForeignKey("ParentModule")]
        public long? ParentModuleId { get; set; }
        public virtual Module ParentModule{ get; set; }


    }
}
