﻿
using Domain.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.Models
{
    [Table("PlanPackages")]
    public class PlanPackage : EntityBase, IEntity
    {
        [MaxLength(1024)]
        public string Description { get; set; }
        [Required]
        public decimal BaseValue { get; set; }
                
    }
}
