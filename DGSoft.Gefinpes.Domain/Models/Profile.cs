﻿using Domain.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.Models
{
    [Table("Profiles")]
    public class Profile : EntityBase, IEntity
    {
        [ForeignKey("Module")]
        public long? ModuleId { get; set; }
        public virtual Module Module { get; set; }

    }
}
