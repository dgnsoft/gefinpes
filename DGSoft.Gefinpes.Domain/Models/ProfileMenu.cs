﻿using Domain.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.Models
{
    [Table("ProfileMenus")]
    public class ProfileMenu : EntityBase, IEntity
    {
    
        [Required, ForeignKey("Profile")]
        public long ProfileId { get; set; }
        public virtual Profile Profile { get; set; }

        [Required, ForeignKey("Menu")]
        public long MenuId { get; set; }
        public virtual Menu Menu { get; set; }

        [Required]
        public bool View { get; set; }
        [Required]
        public bool Create { get; set; }
        [Required]
        public bool Edit { get; set; }
        [Required]
        public bool Delete { get; set; }
    }
}
