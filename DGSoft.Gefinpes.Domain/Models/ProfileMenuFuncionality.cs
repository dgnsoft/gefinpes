﻿using Domain.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.Models
{
    [Table("ProfileMenuFuncionalities")]
    public class ProfileMenuFuncionality : EntityBase, IEntity
    {
        [ForeignKey("Profile")]
        public long ProfileId { get; set; }
        public virtual Profile Profile { get; set; }

        [ForeignKey("MenuFuncionality")]
        public long MenuFuncionalityId { get; set; }
        public virtual MenuFuncionality MenuFuncionality { get; set; }

        [Required]
        public bool Granted { get; set; }
    }
}
