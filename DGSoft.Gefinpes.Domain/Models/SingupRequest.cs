﻿
using Domain.Models.Interfaces;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static Domain.Core.Enums;

namespace Domain.Models
{
    [Table("SignUpRequests")]
    public class SignupRequest : EntityBase
    {
        //Step 1
        [MaxLength(120), Required]
        public string EmailAddress { get; set; }

        [MaxLength(500), Required]
        public string Password { get; set; }

        [MaxLength(20), Required]
        public string PhoneNumber { get; set; }

        [Required]
        public bool IsWhatsapp { get; set; }

        [MaxLength(100), Required]
        public string Name { get; set; }

        //Step 2 - About your Employee
       
        [MaxLength(100), Required]
        public string TradingName { get; set; }
        
        public int? Type { get; set; } //1 - PF ou 2 - PJ

        [MaxLength(50)]
        public string IndividualRegistration { get; set; } //CPF ou CNPJ

        [MaxLength(100)]
        public string CityName { get; set; }

        [MaxLength(500)]
        public string EmployeeDescription { get; set; }

        public SignupRequestStatus SignupRequestStatus { get; set; }

        [MaxLength(500)]
        public string SingupRequestKey { get; set; }

        [ForeignKey("Tenant")]
        public long? TenantId { get; set; }
        [JsonIgnore]
        public virtual Tenant Tenant { get; set; }
    }
}
