﻿using Domain.Core;
using Domain.Models;
using Domain.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static Domain.Core.Enums;

namespace Domain.Models
{
    [Table("SystemParams")]
    public class SystemParam : EntityBase, IEntity
    {
        
        [MaxLength(60),Required]
        public string Key { get; set; }
        
        public Enums.SystemParamType Type { get; set; }
        
        [MaxLength(256), Required]
        public string Description { get; set; }
        [MaxLength(4000)]
        public string Value { get; set; }

    }
}
