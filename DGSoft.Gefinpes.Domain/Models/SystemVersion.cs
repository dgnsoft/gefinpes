﻿using Domain.Models;
using Domain.Models.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    [Table("SystemVersions")]
    public class SystemVersion : EntityBase
    {
    
        [MaxLength(4000)]
        public string Note{ get; set; }
        
        [Required]
        public int VersionMajor { get; set; }
        [Required]
        public int VersionMinor { get; set; }

        [MaxLength(255)]
        public string SqlCreateVersionDatabase { get; set; }

        [MaxLength(255)]
        public string SqlUpdateVersionDatabase { get; set; }

        [MaxLength(255)]
        public string UrlFrontEndAddress { get; set; }
        [MaxLength(255)]
        public string UrlBackEndAddress { get; set; }
        
        [ForeignKey("SystemVersionStatus")]
        public long SystemVersionStatusId { get; set; }
        public virtual SystemVersionStatus SystemVersionStatus { get; set; }
        [ForeignKey("Module")]
        public long ModuleId { get; set; }
        public virtual Module Module { get; set; }

        //TODO Daniel G -> Index to ModuleId, VersionMajor, VersionMinor unique on table
        [NotMapped]
        public decimal FullVersionCode
        {
            get { return Convert.ToDecimal($"{VersionMajor},{VersionMinor}"); }

        }
    }
}
