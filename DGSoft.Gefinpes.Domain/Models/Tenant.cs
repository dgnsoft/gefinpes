﻿using Domain.Models.Interfaces;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.Models
{
    [Table("Tenants")]
    public class Tenant : EntityBase
    {                         
        [MaxLength(50), Required]
        public string DatabaseName { get; set; }
        
        [ForeignKey("AdminUser")]
        public long? AdminUserId { get; set; }
        [JsonIgnore]
        public virtual User AdminUser { get; set; }

        [ForeignKey("CurrrentSystemVersion")]
        public long CurrrentSystemVersionId { get; set; } //Only will be used in Custommers tenants
        public virtual SystemVersion CurrrentSystemVersion { get; set; }


    }
}
