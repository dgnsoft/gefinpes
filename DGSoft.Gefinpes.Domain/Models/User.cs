﻿using Domain.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.Models
{
    [Table("Users")]
    public class User : EntityBase
    {    
        [MaxLength(100),Required]
        public string Name { get; set; }

        [MaxLength(120), Required]
        public string Login { get; set; }
               
        public int UserType { get; set; } //1= administrator / 2 - user

        [ForeignKey("CurrentTenant")]
        public long? CurrentTenantId { get; set; }
        public virtual Tenant CurrentTenant { get; set; }

        [MaxLength(500),Required]
        public string Password { get; set; }

        [Required]
        public bool ChangePassword { get; set; }
        [ForeignKey("CurrentLanguage")]
        public long? CurrentLanguageId { get; set; }
        public virtual Language CurrentLanguage { get; set; }

        [ForeignKey("Module")]
        public long ModuleId { get; set; }
        public virtual Module Module { get; set; }

        [ForeignKey("Profile")]
        public long? ProfileId { get; set; }
        public virtual Profile Profile { get; set; }

        public long? ExternalCustommerUserId { get; set; } //In Admin represents the ID for user register in tenant database
    }
}
