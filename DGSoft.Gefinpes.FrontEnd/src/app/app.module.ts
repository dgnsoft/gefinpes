import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { RouterModule } from "@angular/router";

import { AppRoutingModule } from "./app.routing";

import { AppComponent } from "./app.component";

import { LoginLayoutComponent } from "./layouts/login-layout/login-layout.component";
import { AuthenticationService } from "./shared/service/authentication-service";

import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { HttpErrorInterceptor } from "./interceptors/http-error-interceptor";
import { TokenInterceptor } from "./interceptors/token-interceptor";
import { ToastrModule } from "ngx-toastr";

import { MessageService } from "./shared/service/message-service";
import { BlockUIModule } from "ng-block-ui";
import { TimePointService } from "./shared/service/timePoint-service";

import {
  MAT_DATE_LOCALE,
  DateAdapter,
  MAT_DATE_FORMATS
} from "@angular/material/core";
import {
  MomentDateAdapter
} from "@angular/material-moment-adapter";
import { UserService } from "./shared/service/user-service";
import { TimeReportService } from "./shared/service/time-report.service";
import { AppMaterialModule } from "./material-module";
import { MatToolbarModule } from "@angular/material/toolbar";
import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { FooterComponent } from "./layouts/admin-layout/footer/footer.component";
import { SidebarComponent } from "./layouts/admin-layout/sidebar/sidebar.component";
import { NavbarComponent } from "./layouts/admin-layout/navbar/navbar.component";
import { SharedModule } from "./shared/shared.module";
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { NgParticlesModule } from "ng-particles";
import { ProfileService } from "./shared/service/profile-service";
import { MenuService } from "./shared/service/menu-service";
import { UtilityService } from "./shared/service/utility-service";


export const MY_FORMATS = {
  parse: {
    dateInput: "DD/MM/YYYY"
  },
  display: {
    dateInput: "DD/MM/YYYY",
    monthYearLabel: "MM YYYY",
    dateA11yLabel: "DD/MM/YYYY",
    monthYearA11yLabel: "MM YYYY"
  }
};

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    MatSlideToggleModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: "toast-top-right",
      preventDuplicates: true,
      closeButton: true,
      enableHtml: true,
      progressBar: true,
      progressAnimation: "decreasing",
      disableTimeOut: false
    }),
    BlockUIModule.forRoot(),
    AppMaterialModule,
    MatToolbarModule,
    SharedModule,
    NgParticlesModule    
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoginLayoutComponent,
    FooterComponent,
    SidebarComponent,
    NavbarComponent
  ],
    exports: [FooterComponent, SidebarComponent, NavbarComponent],
  providers: [
    AuthenticationService,
    MessageService,
    TimePointService,
    UserService,
    ProfileService,   
    UtilityService, 
    MenuService,
    TimeReportService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
      deps: [MessageService]
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
      deps: [AuthenticationService]
    },
    { provide: MAT_DATE_LOCALE, useValue: "pt-BR" }, //you can change useValue
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
