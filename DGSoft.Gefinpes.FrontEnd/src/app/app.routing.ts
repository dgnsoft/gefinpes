import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { Routes, RouterModule } from "@angular/router";

import { LoginLayoutComponent } from "./layouts/login-layout/login-layout.component";
import { AuthGuard } from "./shared/authguard/auth-guard";
import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";

const routes: Routes = [
  {
    path: "login",
    component: LoginLayoutComponent,
    loadChildren: "./layouts/login-layout/login-layout.module#LoginLayoutModule"
  },
  {
    path: "",
    component: AdminLayoutComponent,
    children: [
      {
        path: "",
        loadChildren:
          "./layouts/admin-layout/admin-layout.module#AdminLayoutModule"
      }
    ],
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [CommonModule, BrowserModule, RouterModule.forRoot(routes)],
  exports: [],
  providers: [AuthGuard]
})
export class AppRoutingModule {}
