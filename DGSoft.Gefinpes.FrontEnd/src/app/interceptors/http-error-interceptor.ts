import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { MessageService } from "../shared/service/message-service";
import { Injector } from "@angular/core";
import { BlockUI, NgBlockUI } from "ng-block-ui";

export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(public message: MessageService) {}
  @BlockUI() blockUI: NgBlockUI;
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        
        let showModal=false;
        let errMsg:string = "";

        if(error.status == 0 ){
          errMsg = `Não foi possível conectar com o servidor. Erro: ${error.message}`;
        } else {
          var data = error.error;        
          let qtdErros:number = 0;
          if(data.hasOwnProperty("errors")){
            qtdErros = data.errors.length;
          }
          switch(qtdErros){
            case 0: 
                errMsg += `Error Code: ${error.status},  Message: ${error.message}`;
                break;
              case 1: 
                errMsg = data.errors[0];
                break;
              default:
                showModal = true; 
                errMsg+="<ul>";
                data.errors.forEach((err: string) => {
                  errMsg += "<li>" + err + "</li>";
                });
                errMsg += "<ul>";
                break;              
          }
        }
        if(showModal){
          this.message.showModal("error", "Atenção", errMsg);
        } else {
          this.message.showError("Algo deu errado!", errMsg);
        }
        this.blockUI.stop();
        return throwError(error);
      })
    );
  }
}
