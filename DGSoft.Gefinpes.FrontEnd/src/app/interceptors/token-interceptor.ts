import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders,
  HttpResponse,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { AuthenticationService } from "app/shared/service/authentication-service";
import { Injectable } from "@angular/core";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { finalize, map } from "rxjs/operators";
import { catchError } from "rxjs/internal/operators/catchError";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  @BlockUI() blockUI: NgBlockUI;

  constructor(private auth: AuthenticationService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.blockUI.start();
    let currentUser = this.auth.userLogged();
    const headerSettings: { [name: string]: string | string[] } = {};
    let changedRequest = req;

    for (const key of req.headers.keys()) {
      headerSettings[key] = req.headers.getAll(key);
    }
    headerSettings["Content-Type"] = "application/json";
    
    if (currentUser && currentUser.access_token) {      
      headerSettings["Authorization"] = "Bearer " + currentUser.access_token;
    } else {
      
    }
    const newHeader = new HttpHeaders(headerSettings);

    changedRequest = req.clone({
      headers: newHeader
    });
    return next.handle(changedRequest).pipe(
      map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
              //console.log('event--->>>', event);
          }
          return event;
      }),
      catchError((error: HttpErrorResponse) => {
          let data = {};
          data = {
              reason: error && error.error && error.error.reason ? error.error.reason : '',
              status: error.status
          };
          //this.errorDialogService.openDialog(data);
          return throwError(error);
      }),
      finalize(()=>{
        this.blockUI.stop();
      }))
  }
}
