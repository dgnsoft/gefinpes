import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MainComponent } from "./main/main.component";
import { MenusComponent } from "./menus/menus.component";
import { ProfilesComponent } from "./profiles/profiles.component";

export const AdminLayoutRoutes: Routes = [
  { path: "", component: MainComponent },
  { path: "profiles", component: ProfilesComponent },
  { path: "menus", component: MenusComponent }
];
