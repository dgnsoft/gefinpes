import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AppMaterialModule } from "app/material-module";
import { MainComponent } from "./main/main.component";
import { RouterModule } from "@angular/router";
import { AdminLayoutRoutes } from "./admin-layout-routing.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { AppPrimeNgModule } from "app/prime-ng-module.";
import { SharedModule } from "../../shared/shared.module";
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { ProfilesComponent } from './profiles/profiles.component';
import { MenusComponent } from './menus/menus.component';

// import { InputDateComponent } from "app/shared/components/Inputs/input-date/input-date.component";
@NgModule({
    declarations: [MainComponent, ProfilesComponent, MenusComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    AppMaterialModule,
    AppPrimeNgModule,
    FormsModule,
      ReactiveFormsModule,
      SharedModule,
     MatSlideToggleModule
  ]
})
export class AdminLayoutModule {}
