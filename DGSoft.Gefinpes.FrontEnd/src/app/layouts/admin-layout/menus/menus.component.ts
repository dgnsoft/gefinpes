import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ColumnDefintion } from 'app/shared/components/custom-table/column-defintion';
import { CONSTANTS } from 'app/shared/constants';
import { FieldTO, FieldType } from 'app/shared/intefaces/table-config';
import { Menu } from 'app/shared/model/menu';
import { MenuService } from 'app/shared/service/menu-service';
import { UtilityService } from 'app/shared/service/utility-service';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.css']
})
export class MenusComponent implements OnInit {
  
  constructor(private utilityService:UtilityService,
              private service:MenuService) { }
  controller = CONSTANTS.MENU_CONTROLLER;              
  data: Array<Menu>;
  config:Array<FieldTO>;
  displayedColumns: string[] = ['id', 'name'];
  columnsDef: Array<ColumnDefintion>;

  ngOnInit(): void {
    this.service.findAll().subscribe(res => {
      this.data = res.result;  
    });
  }

  

}
