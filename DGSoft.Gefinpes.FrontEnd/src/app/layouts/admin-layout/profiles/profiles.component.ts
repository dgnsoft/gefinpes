import { Component, OnInit } from '@angular/core';
import { CONSTANTS } from 'app/shared/constants';
import { FieldTO } from 'app/shared/intefaces/table-config';
import { Profile } from 'app/shared/model/profile';
import { ProfileService } from 'app/shared/service/profile-service';
import { ResourceService } from 'app/shared/service/resource-service';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.css']
})
export class ProfilesComponent implements OnInit {

  constructor(private profileService:ProfileService) { }
  data: Array<Profile>;
  fields:Array<FieldTO>;
  controller = CONSTANTS.PROFILE_CONTROLLER;              
  displayedColumns=[];
  
  ngOnInit(): void {
    this.profileService.findAll().subscribe(res => {
      this.data = res.result;    
    });
  }

}
