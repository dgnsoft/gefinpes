import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "app/shared/service/authentication-service";

declare const $: any;

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"]
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(private auth: AuthenticationService) {}

  ngOnInit() {
    this.menuItems = this.auth.getMenus().filter(menuItem => menuItem);
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }

  isSidebarOpen() {
    return true;
  }
}
