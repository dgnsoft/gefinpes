import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LoginLayoutRoutes } from "./login-layout.routing.module";
import { LoginComponent } from "./login/login.component";
import {MatDatepickerModule} from '@angular/material/datepicker';
import { AppMaterialModule } from "app/material-module";
import {MatFormFieldModule} from '@angular/material/form-field';
import { NgParticlesModule } from "ng-particles";



@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(LoginLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    AppMaterialModule, MatDatepickerModule, MatFormFieldModule, NgParticlesModule
  ]
})
export class LoginLayoutModule {}
