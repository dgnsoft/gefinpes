import { Component, OnInit } from "@angular/core";
import { Authentication } from "app/shared/model/authentication";
import { AuthenticationService } from "app/shared/service/authentication-service";
import { Router } from "@angular/router";
import { MessageService } from "app/shared/service/message-service";
import { Container } from "@angular/compiler/src/i18n/i18n_ast";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  auth: Authentication = new Authentication();   

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private messages: MessageService
  ) {
    
    
  }
  makeLogin() {
    /*this.messages.showConfirmModal("Aviso","Deseja continuar?").then((result)=> {
      if(result.value){
        
      }
    });*/
    this.authenticationService
        .login(this.auth.login, this.auth.password)
        .subscribe(result => {
          if (result.access_token) {
            this.router.navigate(["/"]);
          }
        });
    
    /*var u = this.authenticationService.login(
      this.auth.username,
      this.auth.password
    );
    if (u && u.token) {
      this.router.navigate(["/"]);
    }*/
  }

  ngOnInit() {
    this.auth.login = "admin";
    this.auth.password="850510";

  }
  id="tsparticles";

  particlesOptions = {
    backgroundMode: {
      enable: true
    },
    background: {
      color: {
        value: "#060646"
      }
    },
    fpsLimit: 30,
    interactivity: {
      detectsOn: "canvas",
      events: {
        onClick: {
          enable: true,
          mode: "push"
        },
        onHover: {
          enable: true,
          mode: "repulse",
          parallax: {
            enable: false,
            force: 60, 
            smooth:10
          }
        },
        resize: true
      },
      modes: {
        bubble: {
          distance: 400,
          duration: 2,
          opacity: 0.8,
          size: 40
        },
        push: {
          quantity: 24
        },
        repulse: {
          distance: 200,
          duration: 0.4
        }
      }
    },
    particles: {
      color: {
        value: "#ffffff"
      },
      links: {
        color: "#ffffff",
        distance: 150,
        enable: true,
        opacity: 0.5,
        width: 1
      },
      collisions: {
        enable: true
      },
      move: {
        direction: "none",
        enable: true,
        outMode: "bounce",
        random: false,
        speed: 6,
        straight: false
      },
      number: {
        density: {
          enable: true,
          value_area: 800
        },
        value: 80
      },
      opacity: {
        value: 0.5
      },
      shape: {
        type: "circle"
      },
      size: {
        random: true,
        value: 5
      }
    },
    detectRetina: true
  };

 particlesLoaded(container: Container): void {
        console.log(container);
    }
  
}
