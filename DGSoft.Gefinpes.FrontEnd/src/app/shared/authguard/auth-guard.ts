import { CanActivate } from "@angular/router";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate() {
    var user = JSON.parse(sessionStorage.getItem("currentUser"));
    if (user && user.access_token) {
      return true;
    }

    this.router.navigate(["/login"]);
    return false;
  }
}
