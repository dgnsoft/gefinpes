import { Component, OnInit, Input, Output, EventEmitter, DoCheck, OnChanges } from '@angular/core';
import { convertMetaToOutput } from '@angular/compiler/src/render3/util';

@Component({
  selector: 'app-input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.scss']
})
export class InputDateComponent implements OnInit, DoCheck, OnChanges {
  @Output() dateChange  = new EventEmitter<Date>()
  @Input() Label: String;
  @Input() InsertDate: Date;

  DescricaoLabel: String;
  SelectDate: Date;
  constructor() { }

  ngOnInit() {
    this.DescricaoLabel = this.Label;
    
  }

  ngDoCheck() {
    this.dateChange.emit(this.SelectDate);
    // this.dateChange = this.SelectDate;
  }
  
  ngOnChanges(){
        this.SelectDate = this.InsertDate; 
  }

}
