import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FieldTO, FieldType } from 'app/shared/intefaces/table-config';
import { UtilityService } from 'app/shared/service/utility-service';
import * as moment from 'moment'
import { map } from 'rxjs/operators';
import { ColumnDefintion } from '../column-defintion';
import { RowActionButtonsComponent } from './row-action-buttons/row-action-buttons.component';


@Component({
  selector: 'app-ag-grid-table',
  templateUrl: './ag-grid-table.component.html',
  styleUrls: ['./ag-grid-table.component.css']
})
export class AgGridTableComponent implements AfterViewInit{

  @Input() data:Array<any>;
  @Input() displayedColumns:Array<string>;
  @Input() baseController:string;
  private gridApi;
  private gridColumnApi;

  rowData: any[];

  frameworkComponents: [
    btnCellRenderer: RowActionButtonsComponent
  ]; 

  columnDefs;
  defaultColDef = {
    width: 150,
    editable: true,
    filter: 'agTextColumnFilter',
    floatingFilter: false,
    resizable: true,
  };

  columnTypes = {
    numberColumn: {
      width: 80,
      filter: 'agNumberColumnFilter',
    },
    medalColumn: {
      width: 100,
      columnGroupShow: 'open',
      filter: false,
    },
    nonEditableColumn: { editable: false },
    booleanColumn: {
      cellRenderer: (data) => {        
        return (data.value==1)?"Sim":"Não";
      },
    },
    dateColumn: {
      cellRenderer: (data) => {        
        return moment(data.value).format('DD/MM/YYYY')
      },
      filter: 'agDateColumnFilter',
      filterParams: {
        comparator: function (filterLocalDateAtMidnight, cellValue) {        

          var cellDate = new Date(cellValue);
          if (cellDate < filterLocalDateAtMidnight) {
            return -1;
          } else if (cellDate > filterLocalDateAtMidnight) {
            return 1;
          } else {
            return 0;
          }
        },
      },
    },
  }

  constructor(private utilityService:UtilityService, private http: HttpClient) { 
    
  }
  ngAfterViewInit(): void {
    //throw new Error('Method not implemented.');
  }
  
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    console.log(this.gridApi);
    var columnDefs = this.gridApi.getColumnDefs();
    let colDefActions:ColumnDefintion = new ColumnDefintion();
    colDefActions.sortable=true;
    colDefActions.editable=false;
    colDefActions.headerName = '';
    colDefActions.field = 'id';
    colDefActions.filter = false;
    colDefActions.cellRendererFramework = RowActionButtonsComponent;
    columnDefs.push(colDefActions);

    this.utilityService.fieldsDescription(this.baseController).subscribe(res => {
      let config = res.result;
      if(config && config.length>0){
        config.forEach(field => {
          let colDef:ColumnDefintion = new ColumnDefintion();
          colDef.sortable=true;
          colDef.editable=false;
          colDef.headerName = field.label;
          colDef.field = field.fieldName;
          colDef.filter = false;
          let type:string = '';
          switch(field.fieldType){
            case FieldType.DATETIME: 
              colDef.filter = "agDateColumnFilter";
              type='dateColumn';
              break;
            case FieldType.NUMBER:
              colDef.filter = "agNumberColumnFilter";
              type='numberColumn';
              break;
             case FieldType.BOOLEAN:
                colDef.filter = "agNumberColumnFilter";
                type='booleanColumn';
                break;
            default:
              colDef.filter =  "agTextColumnFilter"
              break;
          }
          colDef.filter =false;
          colDef.type = type;
          columnDefs.push(colDef);
        });
        this.columnDefs = columnDefs;
      }      
    }); 
    
    
  }

  

 
};

