import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular/lib/interfaces';
import { ICellRendererParams, IAfterGuiAttachedParams } from 'ag-grid-community';



@Component({
  selector: 'app-row-action-buttons',
  templateUrl: './row-action-buttons.component.html',
  styleUrls: ['./row-action-buttons.component.css']
})
export class RowActionButtonsComponent implements ICellRendererAngularComp {
  @Output() editEvent: EventEmitter<number>;
  @Output() removeEvent: EventEmitter<number>;
  private params: any;

  constructor() { }

  refresh(params: ICellRendererParams): boolean {
    return true;
  }
  afterGuiAttached?(params?: IAfterGuiAttachedParams): void {
  
  }
  
  agInit(params: any): void {
    this.params = params;
  }

  edit() {
    var val = this.params.value;
    console.log("Edit Clicked:" + val);
    alert("Edit Clicked:" + val);
    this.editEvent.emit(val);
  }

  remove() {
    var val = this.params.value;   
    console.log("Remove Clicked:" + val); 
    alert("Remove Clicked:" + val);
    this.removeEvent.emit(val);
  }
  ngOnDestroy() {
    // no need to remove the button click handler 
    // https://stackoverflow.com/questions/49083993/does-angular-automatically-remove-template-event-listeners
  }

}
