export class ColumnDefintion {
    headerName:string;
    field:string;
    filter:any;
    type: any;
    editable:boolean;
    width: number;
    sortable:boolean;
    cellRenderer:any;
    cellRendererFramework: any;
}