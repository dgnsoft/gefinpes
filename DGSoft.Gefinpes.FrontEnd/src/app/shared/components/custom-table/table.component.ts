import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FieldTO, FieldType } from 'app/shared/intefaces/table-config';
import { UtilityService } from 'app/shared/service/utility-service';
import * as moment from 'moment'
import { map } from 'rxjs/operators';
import { ColumnDefintion } from './column-defintion';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements AfterViewInit{

  @Input() data:Array<any>;
  @Input() displayedColumns:Array<string>;
  @Input() baseController:string;
  
  constructor(private utilityService:UtilityService, private http: HttpClient) { 
    
  }
  ngAfterViewInit(): void {
    throw new Error('Method not implemented.');
  }
   

 
};

