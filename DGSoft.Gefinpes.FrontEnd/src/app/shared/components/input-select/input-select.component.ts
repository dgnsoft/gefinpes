import { Component, OnInit, Input, Output, EventEmitter, DoCheck, OnChanges } from '@angular/core';
import { Select } from 'app/shared/model/Select';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-input-select',
  templateUrl: './input-select.component.html',
  styleUrls: ['./input-select.component.scss']
})
export class InputSelectComponent implements OnInit, DoCheck, OnChanges {

  @Output() OutputData  = new EventEmitter<String>()

  @Input() InputLabel: String;
  @Input() InputName: String ;
  @Input() InputListData: Array<Select> = [];
  @Input() InputData: number;
  @Input() InputRequired: any;

  Label: String;
  Name: String;
  NgModel: any;
  ListObj: Array<Select> = [];  
  teste:any;
  FormControl: FormControl
  NgRequired: boolean;

  constructor() { }

  ngOnInit() {    
     this.EventInit()
  }

  ngDoCheck() {
    this.OutputData.emit(this.NgModel);
  }
  
  ngOnChanges(){
        this.NgModel = this.InputData; 
  }

  EventInit = function () {
    this.ListObj = this.InputListData;
    this.Label = this.InputLabel;
    this.Name = this.InputName;
    if(this.InputRequired == "true"){
      this.NgRequired = true;
    }
    else{
      this.NgRequired = false;
    }
   
    this.FormControl = new FormControl('', [
      Validators.nullValidator,
    ]);
  }

}
