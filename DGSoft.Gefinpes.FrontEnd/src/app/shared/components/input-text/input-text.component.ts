import { Component, OnInit, Input, Output, EventEmitter, DoCheck, OnChanges } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}


@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss']
})
export class InputTextComponent implements OnInit, DoCheck, OnChanges {
  @Output() OutputData  = new EventEmitter<String>()

  @Input() InputPlaceholder: String;
  @Input() InputLabel: String;
  @Input() InputName: String ;
  @Input() InputData: String;
  @Input() InputValidate: String;

  formControl: String;
  type: String;
  placeholder: String;
  Label: String;
  Name: String;
  NgModel: String;
  Validate: boolean = false;
  FormControl: FormControl

  matcher = new MyErrorStateMatcher();

  constructor() { }

  ngOnInit() {
    this.placeholder = this.InputPlaceholder
    this.Label = this.InputLabel
    this.Name = this.InputName
    this.VerificarValidacao()

  }
  ngDoCheck() {
    this.OutputData.emit(this.NgModel);
  }
  
  ngOnChanges(){
        this.NgModel = this.InputData; 
  }

  VerificarValidacao = function() {
    if(this.InputValidate == "true"){
      this.Validate = true;
      this.FormControl = new FormControl('', [
        Validators.required,
      ]);
      
    }
    else{
      this.FormControl = new FormControl('', [
        Validators.nullValidator,
      ]);
    }
  }


}
