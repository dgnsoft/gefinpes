export class FieldTO {
    fieldName:string;
    fieldType: FieldType
    tabIndex:number
}

export enum FieldType {
    STRING,
    DATE,
    DATETIME,
    NUMBER,
    BOOLEAN
}