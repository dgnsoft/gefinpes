export class BaseModel {
  id: number;
  active: boolean;
  createdById:number;
  createdBy:string;
  createdAt:Date;
  updatedById:number;
  updatedBy:string;
  updatedAt:Date;
}
