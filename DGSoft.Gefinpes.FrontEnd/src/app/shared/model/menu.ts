import { BaseModel } from "./base-model";

export class Menu extends BaseModel {
    name:string;
    moduleId:number;
}