export class ResponseModel<T> {
  result: Array<T>;
  success: boolean;
  message: Array<String>;
  exceptionLevel: number;
  exceptionType: number;
}
