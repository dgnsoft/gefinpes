export interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
  visible: boolean;
  iconColor: string;
}
