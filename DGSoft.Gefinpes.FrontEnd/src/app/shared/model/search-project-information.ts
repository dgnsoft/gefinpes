import { BaseModel } from "./base-model";

export class SearchProjectInformation extends BaseModel {
    name: string;
    description: string;
    indetifier: string;
    status: number;
}