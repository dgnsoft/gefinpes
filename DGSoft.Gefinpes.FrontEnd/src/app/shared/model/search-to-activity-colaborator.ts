export class SearchToActivityColaborator {
    userId: number;
    projectId: number;
    status: string;
    startDate: Date;
    endDate: Date;
    issueId: number;
}