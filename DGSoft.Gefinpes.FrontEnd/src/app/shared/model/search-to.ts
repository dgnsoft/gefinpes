export class SearchTO {
  userId: number;
  startDate: Date;
  endDate: Date;
  firstName: string;
}
