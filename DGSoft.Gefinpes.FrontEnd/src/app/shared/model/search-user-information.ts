import { BaseModel } from "./base-model";

export class SearchUserInformation extends BaseModel {
  login: string;
  firstName: string;
  lastName: string;
  fullName: string;
  admin: boolean;
  status: number;
  roles: Array<string>;
  token: string;
  isSuccessAuth: boolean;
  ordenationSelectedValue:string;
} 