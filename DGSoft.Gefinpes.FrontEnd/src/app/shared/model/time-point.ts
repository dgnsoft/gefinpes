import { BaseModel } from "./base-model";

export class TimePoint extends BaseModel {
  codFuncionario: string;
  dataPonto: Date;
  horaPonto: string;
  status: boolean;
  codFuncao: number;
  relogio: number;

  constructor(
    dataPonto: Date,
    horaPonto: string,
    codFuncao: number,
    relogio: number
  ) {
    super();
    this.dataPonto = dataPonto;
    this.horaPonto = horaPonto;
    this.codFuncao = codFuncao;
    this.relogio = relogio;
  }
}
