import { BaseModel } from "./base-model";

export class TimeReportActivityColaboratorDto extends BaseModel {
    userId: number;
    userLogin: string;
    date: Date;
    project: string;
    hoursWorked: number;
    issueId: number;
    userFirstName: string;
    userLastName: string;
    issueName: string;
    approverLogin: string;
    approver: string;
    timeStart: string;
    timeEnd: string;
    status: string;
    activityPerformed: string;
    comments: string;
    fullName: string;
}