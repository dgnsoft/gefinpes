import { User } from "./user";
import { BaseModel } from "./base-model";

export class UserByStatus extends BaseModel {
  status: number;
  nomeStatus: string;
  fullName: string;
  login: string;

  constructor(
    id: number,
    status: number,
    nomeStatus: string,
    fullName: string,
    login: string
  ) {
    super();
    this.status = status;
    this.id = id;
    this.nomeStatus = nomeStatus;
    this.login = login;
    this.fullName = fullName;
  }
}
