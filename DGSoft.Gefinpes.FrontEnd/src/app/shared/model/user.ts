import { BaseModel } from "./base-model";

export class User extends BaseModel {
  name: string;
  login: string;
  userType: number;
  currentTenantId: number;
  tenantName: string;
  databaseName: string;
  moduleId: number;
  access_token: string;
  profileId: number;
  profileName: string;
  loginDate: Date;
}
