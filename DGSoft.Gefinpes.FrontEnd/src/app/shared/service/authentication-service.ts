import { User } from "../model/user";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { map, catchError, finalize } from "rxjs/operators";
import { Router } from "@angular/router";
import { Authentication } from "../model/authentication";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { RouteInfo } from "../model/route-info";
import { ResponseModel } from "../model/response-model";

@Injectable()
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  @BlockUI() blockUI: NgBlockUI;

  isAdmin: boolean = false;
  isRh: boolean = false;
  isFinanceiro: boolean = false;
  isGerente: boolean = false;

  private httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"
    })
  };

  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(sessionStorage.getItem("currentUser"))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public userLogged() {
    let u: User = JSON.parse(sessionStorage.getItem("currentUser"));
    return u;
  }

  public tokenUser() {
    let u: User = this.userLogged();
    if (u != null) {
      return u.access_token;
    } else {
      return null;
    }
  }

  login(username: string, password: string) {
    var url = environment.apiUrl + "Auth/login";
    let auth: Authentication = new Authentication();
    
    auth.login = username;
    auth.password = password;
   // this.blockUI.start();
    return this.http.post<ResponseModel<User>>(url, auth, this.httpOptions).pipe(
      map(res => {
        // login successful if there's a jwt token in the response        
        var u = res.result[0];        
        if (u && u.access_token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          sessionStorage.setItem("currentUser", JSON.stringify(u));
          this.currentUserSubject.next(u);
          this.isAdmin = this.hasRole("ROLE_ADMINISTRADOR");
          this.isRh = this.hasRole("ROLE_RH");
          this.isGerente = this.hasRole("ROLE_GERENTE");
          this.isFinanceiro = this.hasRole("ROLE_FINANCEIRO");
        }

        return u;
      }),
      finalize(() => {
       // this.blockUI.stop();
      })
    );
    /*var u = new User();
    u.admin = true;
    u.firstName = "Admin";
    u.login = "admin";
    u.token = "asasdfdafsadf" + new Date().getTime();
    +"ddflsjflas-faslfjalsd";
    this.blockUI.stop();

    sessionStorage.setItem("currentUser", JSON.stringify(u));
    this.currentUserSubject.next(u);

    return u;*/
  }

  logout() {
    // remove user from local storage to log user out
    sessionStorage.removeItem("currentUser");
    this.currentUserSubject.next(null);
    this.router.navigate(["/login"]);
  }

  hasRole(role: string): boolean {
    let u: User = this.currentUserSubject.value;
    let hasRole: boolean = true;
    return hasRole;
  }

  getMenus() {
    let ROUTES: RouteInfo[] = [
      {
        path: "/dashboard",
        title: "Dashboard",
        icon: "dashboard",
        class: "",
        visible: true,
        iconColor: ""
      },
      {
        path: "/profiles",
        title: "Profile",
        icon: "supervisor_account",
        class: "",
        visible: true,
        iconColor: ""
      },
      {
        path: "/menus",
        title: "Menu",
        icon: "supervisor_account",
        class: "",
        visible: true,
        iconColor: ""
      }
    ];
    return ROUTES;
  }
}
