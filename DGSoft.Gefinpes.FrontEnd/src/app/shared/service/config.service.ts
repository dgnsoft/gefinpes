import { HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { Message } from "@angular/compiler/src/i18n/i18n_ast";

@Injectable({
  providedIn: "root"
})
export class ConfigService {
  handleError(error: HttpErrorResponse) {
    let msg: string = "";
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      (msg = "Ocorreu um erro:"), error.error.message;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      msg = `Code: ${error.status}, ` + `Body: ${error.error}`;
    }
    // return an observable with a user-facing error message
    console.error(msg);
    alert(msg);
    return throwError("Algo acontenceu, tente novamente mais tarde");
  }
}
