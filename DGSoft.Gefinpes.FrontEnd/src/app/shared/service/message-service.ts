import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import Swal, { SweetAlertIcon } from "sweetalert2";

@Injectable()
export class MessageService {
  constructor(private toastr: ToastrService) {}

  conf = {
    animate: "scale"
  };

  showSuccess(title: string, message: string) {
    this.toastr.success(message, title);
  }

  showError(title: string, message: string) {
    this.toastr.error(message, title);
  }

  showInfo(title: string, message: string) {
    this.toastr.info(message, title);
  }

  showWarning(title: string, message: string) {
    this.toastr.warning(message, title);
  }

  showAlert(title: string, message: string) {
    this.toastr.show(message, title);
  }

  showModal(type:SweetAlertIcon, title: string, message: string, width:number = 350){
    Swal.fire({        
      icon: type,  
      title: title, 
      html: message,
      width: width
    })  ;
  }

  showConfirmModal(title: string, message: string){
    return  Swal.fire({  
      title: title,  
      text: message,  
      icon: 'success',  
      showCancelButton: true,  
      confirmButtonText: 'Sim',  
      cancelButtonText: 'Não',
      buttonsStyling: false,
      customClass: {
        confirmButton: 'btn btn-success btn-xs',
        cancelButton: 'btn btn-danger btn-xs'
      },
    });
  }
}
