import { ResourceService } from "./resource-service";
import { Project } from "../model/project";
import { SearchProjectInformation } from "../model/search-project-information";
import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { environment } from "environments/environment";
import { map, finalize } from "rxjs/operators";
import { ResponseModel } from "../model/response-model";
import { Profile } from "../model/profile";
import { CONSTANTS } from "../constants";

@Injectable()
export class ProfileService extends ResourceService<Profile> {
  constructor(private client: HttpClient) {
    super(client, CONSTANTS.PROFILE_CONTROLLER);
  }


   
}