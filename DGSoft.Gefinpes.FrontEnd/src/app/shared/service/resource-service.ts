import { BaseModel } from '../model/base-model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JsonPipe } from '@angular/common';
import { environment } from 'environments/environment';
import { map, finalize } from 'rxjs/operators';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Injectable } from '@angular/core';
import { ResponseModel } from '../model/response-model';
import { FieldTO } from '../intefaces/table-config';

export class ResourceService<T extends BaseModel> {
  @BlockUI() blockUI: NgBlockUI;
  constructor(private httpClient: HttpClient, private endpoint: string) {}

  getEndServerUrlFull():string{
    return `${environment.apiUrl}${this.endpoint}`;
  }

  search(item: any): Observable<ResponseModel<T>> {
    return this.httpClient
      .post<ResponseModel<T>>(
        `${this.getEndServerUrlFull()}/search`,
        item,
        {}
      )
      .pipe(
        map(res => {
          // login successful if there's a jwt token in the response
          let json = JSON.stringify(res);
          return JSON.parse(json);
        }),
        finalize(() => {
          this.blockUI.stop();
        })
      );
  }

  findById(id: number){
    return this.httpClient
    .get<ResponseModel<T>>(
      `${this.getEndServerUrlFull()}/${id}`,      
      {}
    )
    .pipe(
      map(res => {
        let json = JSON.stringify(res);        
        return JSON.parse(json);
      }),
      finalize(() => {
        this.blockUI.stop();
      })
    );
  }

  fieldsDescription(){
    return this.httpClient
    .get<ResponseModel<FieldTO>>(
      `${this.getEndServerUrlFull()}/fieldsDefinition`,      
      {}
    )
    .pipe(
      map(res => {
        let json = JSON.stringify(res);        
        return JSON.parse(json);
      }),
      finalize(() => {
        this.blockUI.stop();
      })
    );
  }

  findAll(){
    return this.httpClient
    .get<ResponseModel<T>>(
      `${this.getEndServerUrlFull()}`,      
      {}
    )
    .pipe(
      map(res => {
        let json = JSON.stringify(res);        
        return JSON.parse(json);
      }),
      finalize(() => {
        this.blockUI.stop();
      })
    );
  }
}
