import { ResourceService } from './resource-service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../model/user';

@Injectable()
export class SearchFieldColaboratorService extends ResourceService<User> {
    constructor(private client: HttpClient) {
        super(client, 'user');
    }
}
