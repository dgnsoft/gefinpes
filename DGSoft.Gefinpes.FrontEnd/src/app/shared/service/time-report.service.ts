import { ResourceService } from "./resource-service";
import { TimeReportActivityColaboratorDto } from "../model/time-report-activity-colaborator-dto";
import { SearchToActivityColaborator } from "../model/search-to-activity-colaborator";
import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { environment } from "environments/environment";
import { map, finalize } from "rxjs/operators";

@Injectable()
export class TimeReportService extends ResourceService<TimeReportActivityColaboratorDto> {
  constructor(private client: HttpClient) {
    super(client, "time-report");
   }
}