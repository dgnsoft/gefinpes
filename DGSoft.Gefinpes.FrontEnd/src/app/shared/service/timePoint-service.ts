import { ResourceService } from './resource-service';
import { TimePoint } from '../model/time-point';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class TimePointService extends ResourceService<TimePoint> {
  constructor(private client: HttpClient) {
    super(client, 'time-point');
  }
}
