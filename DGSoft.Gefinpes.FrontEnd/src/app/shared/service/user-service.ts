import { ResourceService } from "./resource-service";
import { User } from "../model/user";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { UserByStatus } from "../model/user-by-status";
import { Observable } from "rxjs";
import { environment } from "environments/environment";
import { map, finalize } from "rxjs/operators";
import { ResponseModel } from "../model/response-model";

export const _constendpointUrl: string = "user";

@Injectable()
export class UserService extends ResourceService<User> {
  constructor(private client: HttpClient) {
    super(client, _constendpointUrl);
  }

  getUsersGrupedByStatus(): Observable<ResponseModel<UserByStatus>> {
    return this.client
      .get<ResponseModel<UserByStatus>>(
        `${environment.apiUrl}/${_constendpointUrl}/grupedByStatus`,
        {}
      )
      .pipe(
        map(res => {
          // login successful if there's a jwt token in the response
          let json = JSON.stringify(res);
          return JSON.parse(json);
        }),
        finalize(() => {
          this.blockUI.stop();
        })
      );
    }


    getSearchUsers(searchUserInformation:User, ordenationSelectedValue: string): Observable<ResponseModel<User>> {
        return this.client
            .post<ResponseModel<User>>(
                `${environment.apiUrl}/${_constendpointUrl}/advancedsearch`, searchUserInformation
            )
            .pipe(
                map(res => {
                    // login successful if there's a jwt token in the response
                    let json = JSON.stringify(res);
                    
                    return JSON.parse(json);
                }),
                finalize(() => {
                    this.blockUI.stop();
                })
            );
    }
    //getSearchUsers(searchUserInformation: User): Observable<Response<User>> {
    //    return this.client
    //        .post<Response<User>>(
    //            `${environment.apiUrl}/${_constendpointUrl}/searchbylogin`, { login: searchUserInformation.login}
    //        )
    //        .pipe(
    //            map(res => {
    //                // login successful if there's a jwt token in the response
    //                let json = JSON.stringify(res);
    //                return JSON.parse(json);
    //            }),
    //            finalize(() => {
    //                this.blockUI.stop();
    //            })
    //        );
    //}
}
