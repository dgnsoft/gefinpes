import { BaseModel } from '../model/base-model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JsonPipe } from '@angular/common';
import { environment } from 'environments/environment';
import { map, finalize } from 'rxjs/operators';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Injectable } from '@angular/core';
import { ResponseModel } from '../model/response-model';
import { FieldTO, FieldType } from '../intefaces/table-config';
import { ColumnDefintion } from '../components/custom-table/column-defintion';

@Injectable()
export class UtilityService{
  @BlockUI() blockUI: NgBlockUI;
  constructor(private httpClient: HttpClient) {}

  getEndServerUrlFull(endpoint:string):string{
    return `${environment.apiUrl}${endpoint}`;
  }

  fieldsDescription(controller:string){
    return this.httpClient
    .get<ResponseModel<FieldTO>>(
      `${this.getEndServerUrlFull(controller)}/fieldsDefinition`,      
      {}
    )
    .pipe(
      map(res => {
        let json = JSON.stringify(res);        
        return JSON.parse(json);
      }),
      finalize(() => {
        this.blockUI.stop();
      })
    );
  }

  getColumnDefintionsFromFields(controller:string){
    let columnsDef = new Array<ColumnDefintion>();
    this.fieldsDescription(controller).subscribe(res => {
      let config = res.result;
      if(config && config.length>0){
        config.forEach(field => {
          let colDef:ColumnDefintion = new ColumnDefintion();
          colDef.sortable=true;
          colDef.editable=false;
          colDef.headerName = field.fieldName;
          colDef.field = field.fieldName;
          let type:string = '';
          switch(field.fieldType){
            case FieldType.DATETIME: 
              colDef.filter = "agDateColumnFilter";
              type='dateColumn';
              break;
            case FieldType.NUMBER:
              colDef.filter = "agNumberColumnFilter";
              type='numberColumn';
              break;
            default:
              colDef.filter =  "agTextColumnFilter"
              break;
          }
          colDef.type = type;
          columnsDef.push(colDef);
        });
      }      
    });
    return columnsDef;    
  }
}
