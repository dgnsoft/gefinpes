import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from "app/material-module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { AppPrimeNgModule } from "app/prime-ng-module.";

import {MatSelectModule} from '@angular/material/select';
import { InputDateComponent } from './components/Inputs/input-date/input-date.component';

import { InputTextComponent } from './components/input-text/input-text.component';
import { InputSelectComponent } from './components/input-select/input-select.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AgGridModule } from 'ag-grid-angular';
import { AgGridTableComponent } from './components/custom-table/ag-grid-table/ag-grid-table.component';
import { TableComponent } from './components/custom-table/table.component';
import { RowActionButtonsComponent } from './components/custom-table/ag-grid-table/row-action-buttons/row-action-buttons.component';
@NgModule({
    declarations: [InputDateComponent
        ,InputTextComponent, InputSelectComponent, TableComponent, AgGridTableComponent, RowActionButtonsComponent],
  imports: [
      CommonModule, AppMaterialModule,AppPrimeNgModule,
      FormsModule, ReactiveFormsModule, MatDialogModule, MatSelectModule,  MatFormFieldModule,
      AgGridModule.withComponents([RowActionButtonsComponent])
    ],
    exports: [InputDateComponent, InputTextComponent, InputSelectComponent, TableComponent, AgGridTableComponent, RowActionButtonsComponent],
    entryComponents:  [InputDateComponent, InputTextComponent, InputSelectComponent]

})
export class SharedModule { }
