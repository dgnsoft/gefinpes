﻿using Domain.Core;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.DAL
{
    public class PersistentContext: DbContext
    {
        public string dataBaseName;
        public long CurrentUserId { get; set; }

        public string CurrentUserLogin { get; set; }
        public PersistentContext()
        { }
        public PersistentContext(string dataBaseName)
        {
            this.dataBaseName = dataBaseName;
        }

        public PersistentContext(DbContextOptions<PersistentContext> options)
            : base(options)
        {
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connection = String.Empty;
            if (string.IsNullOrEmpty(this.dataBaseName))
            {
                connection = GetConnectionString(Constants.CONNECTION_STRING_DEFAULT);
            } else
            {
                connection = $"{GetConnectionString(Constants.CUSTOMMER_STRING_DEFAULT)}Database={this.dataBaseName}";
            }
            
            optionsBuilder.UseLazyLoadingProxies().UseSqlServer(connection, b => b.MigrationsAssembly("DGSoft.Gefinpes.API"));            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Label>().HasIndex(s => s.Key).IsUnique().HasName("idx_uk_label_key");
            modelBuilder.Entity<SystemParam>().HasIndex(s => s.Key).IsUnique().HasName("idx_uk_system_param_key");
            modelBuilder.Entity<Tenant>().HasIndex(s => s.DatabaseName).IsUnique().HasName("idx_uk_tenant_databasename");
            modelBuilder.Entity<SignupRequest>().HasIndex(s => s.SingupRequestKey).IsUnique().HasName("idx_uk_singup_req_key");


            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.ClientNoAction;
            }

            foreach (var property in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetProperties().Where(p => p.ClrType == typeof(string))))
            {
                int maxLengthValue = 255;
                var maxLength = property.GetMaxLength();
                if(maxLength != null)
                {
                    maxLengthValue = maxLength.Value;
                }
                property.SetColumnType($"varchar({maxLength})");               
            }
        }

     

        public static string GetConnectionString(string connectionName)
        {
           IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .AddJsonFile("appsettings.Development.json", optional: true)
            .Build();

           string connection = configuration["ConnectionStrings:"+ connectionName];
           return connection;
        }

        #region "DBSETS"

        public DbSet<Language> Languages { get; set; }
        public DbSet<Label> Labels { get; set; }
        public DbSet<LanguageLabels> LanguagesLabels { get; set; }
        public DbSet<Module> Modules { get; set; }
     
        public DbSet<Menu> Menus { get; set; }

        public DbSet<MenuFuncionality> MenuFuncionalities { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<ProfileMenu> ProfileMenus { get; set; }
        public DbSet<ProfileMenuFuncionality> ProfileMenuFuncionalities { get; set; }

        public DbSet<SystemVersion> SystemVersions { get; set; }
        public DbSet<SystemParam> SystemPramns { get; set; }
        public DbSet<Tenant> Tenants { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Customer> Customers { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<PlanPackage> PlanPackages { get; set; }

        //Messages
        public DbSet<MessageType> MessageTypes { get; set; }
        public DbSet<Message> Messages { get; set; }

        public DbSet<SignupRequest> SignupRequests { get; set; }
        #endregion

        #region "Override methods area"


        public override int SaveChanges()
       {
           return base.SaveChanges();
       }
        public int SaveChangesWithAudit(UserDTO userLoggedData)
        {
            if (userLoggedData != null)
            {
                CurrentUserId = userLoggedData.Id;
                CurrentUserLogin = userLoggedData.Login;
            }
            UpdateAuditEntities();
            return base.SaveChanges();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
       {
           return base.SaveChanges(acceptAllChangesOnSuccess);
       }


       public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
       {
           return base.SaveChangesAsync(cancellationToken);
       }


       public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
       {
           return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
       }


       private void UpdateAuditEntities()
       {
           var modifiedEntries = ChangeTracker.Entries()
               .Where(x => x.Entity is IAuditEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));


           foreach (var entry in modifiedEntries)
           {
               IAuditEntity entity = (IAuditEntity)entry.Entity;
               DateTime now = DateTime.UtcNow;

               if (entry.State == EntityState.Added)
               {
                   entity.CreatedAt = now;
                   entity.CreatedById = CurrentUserId;
                   entity.CreatedBy = CurrentUserLogin;
               }
               else
               {
                    base.Entry(entity).Property(x => x.CreatedAt).IsModified = false;
                    base.Entry(entity).Property(x => x.CreatedById).IsModified = false;
                    base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                }

               entity.UpdatedAt = now;
               entity.UpdatedById = CurrentUserId;
               entity.UpdatedBy = CurrentUserLogin;
           }
       }

#endregion
    }
}
