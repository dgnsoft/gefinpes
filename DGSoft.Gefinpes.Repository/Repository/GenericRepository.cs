﻿
using Domain.Core.Exceptions;
using Domain.DAL.Repository.Interfaces;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Domain.DAL.Repository
{
    public class GenericRepository<T> : IGenericRepository<T>
         where T : EntityBase
    {

        private readonly PersistentContext _context;
        private DbSet<T> entities;

        public GenericRepository(PersistentContext context)
        {
            _context = context;
            entities = this._context.Set<T>();
        }

        public T Create(T entity, UserDTO _userLoggedData = null)
        {
            entities.Add(entity);
            _context.SaveChangesWithAudit(_userLoggedData);
            return entity;
        }

        public void Delete(T entity, UserDTO _userLoggedData = null)
        {
            try
            {
                entities.Remove(entity);
                _context.SaveChanges();                
            } catch(Exception e)
            {                
                string error = $"Error on delete {typeof(T)} with ID: {entity.Id}! Detail: {e.Message}.";
                if(e.InnerException!=null && !string.IsNullOrEmpty(e.InnerException.Message))
                {
                    error += $" / {e.InnerException.Message}";
                }
                throw new DALException(error);
            }
        }

        public void DeleteById(long id, UserDTO _userLoggedData = null)
        {
            try
            {
                var entityToDelete = entities.FirstOrDefault(e => e.Id == id);
                if (entityToDelete != null)
                {
                    entities.Remove(entityToDelete);
                }
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                string error = $"Error on delete record {typeof(T)} with ID: {id}! Detail: {e.Message}.";
                if (e.InnerException != null && !string.IsNullOrEmpty(e.InnerException.Message))
                {
                    error += $" / {e.InnerException.Message}";
                }
                throw new DALException(error);
            }
           
        }

        public T Edit(long id, T entity, UserDTO _userLoggedData = null)
        {
            try
            {
                //Return ServiceException if no exists
                T testEdit = GetById(id);
                _context.Entry(testEdit).State = EntityState.Detached;

                if (testEdit == null)
                {
                    throw new Exception($"Entity {typeof(T)} with id {id} not found");
                }
                else
                {
                    IAuditEntity entityOld = (IAuditEntity)testEdit;
                    ((IAuditEntity)entity).CreatedAt = entityOld.CreatedAt;
                    ((IAuditEntity)entity).CreatedBy = entityOld.CreatedBy;
                    ((IAuditEntity)entity).CreatedById = entityOld.CreatedById;
                }
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChangesWithAudit(_userLoggedData);
                return entity;
            }
            catch (Exception e)
            {
                string error = $"Error on Edit record {typeof(T)} with ID: {id}! Detail: {e.Message}.";
                if (e.InnerException != null && !string.IsNullOrEmpty(e.InnerException.Message))
                {
                    error += $" / {e.InnerException.Message}";
                }
                throw new DALException(error);
            }
            finally { }
            
        }

        public T GetById(long id)
        {
            return entities.FirstOrDefault(e => e.Id == id);
        }

        public IEnumerable<T> Filter()
        {
            return entities;
        }

        public IEnumerable<T> Filter(Func<T, bool> predicate)
        {
            return entities.Where(predicate);
        }

        public void SaveChanges() => _context.SaveChanges();

        public void Edit(long id, T entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> Filter(Expression<Func<T, bool>> expression)
        {
            return entities.Where(expression);
        }
    }
}