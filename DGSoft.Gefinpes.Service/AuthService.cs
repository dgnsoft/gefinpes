﻿using Domain.BLL.Intefaces;
using Domain.DAL;
using Domain.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Domain.Core.Util;
using Domain.Core.Exceptions;
using System.IdentityModel.Tokens.Jwt;
using Domain.Core;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Domain.Models.DTO;
using Microsoft.AspNetCore.Http;
using System.Threading;

namespace Domain.BLL
{
    public class AuthService : IAuthService
    {
        public User GetUserByLogin(string login, long ModuleId)
        {
            User usSearch = null;
            if (String.IsNullOrEmpty(login))
            {
                throw new ServiceException("Enter user login");
            }
            else { }

            PersistentContext _context = new PersistentContext(string.Empty);
            usSearch = _context.Users.Where(p => login.ToUpper().Equals(p.Login)).FirstOrDefault();
            //Validate if user is valid and active
            if (usSearch == null || !usSearch.Active)
            {
                throw new Exception("User not found or inactive on system!");
            }
            else { }

            // validate if user is from module that try to authenticate
            if(usSearch.ModuleId != ModuleId)
            {
                string ModuleName = (ModuleId == (long)Enums.Modules.ProductAdmin ? "Product Admin" : "Custommer");
                
                throw new Exception($"User Hasn't permission on module '{ModuleName}' ");
            }
            else { }

            //validate if user has profile
            if (((int)Enums.Modules.ProductAdmin)==ModuleId && !usSearch.ProfileId.HasValue)
            {
                throw new ServiceException("The user is not linked to a profile!");
            }
            else { }
            return usSearch;
        }

        public UserDTO Authenticate(string login, string password)
        {
            User usSearch = null;
            UserDTO userReturn = null;
            try
            {
                usSearch = GetUserByLogin(login, (long)Enums.Modules.ProductAdmin);              

                //If have some problem when validate user password, return a exception
                CheckUserPassword(usSearch, password);
              
                DateTime loginDate = DateTime.Now;
                //genetare token for user
                userReturn = new UserDTO()
                {
                    Id = usSearch.Id,
                    Name = usSearch.Name,
                    Login = usSearch.Login,
                    UserType = usSearch.UserType,
                    CurrentTenantId = usSearch.CurrentTenantId,
                    ModuleId = usSearch.ModuleId,
                    ModuleName = usSearch.Module.Name,
                    LoginDate = loginDate,
                    DatabaseName = usSearch.CurrentTenant.DatabaseName,
                    TenantName = usSearch.CurrentTenant.Name,
                    ProfileId = usSearch.ProfileId.Value,
                    ProfileName = usSearch.Profile.Name
                };
                String token = GenerateToken(userReturn);
                userReturn.access_token = token;
               
            }
            catch (Exception e)
            {
                throw new ServiceException(e.Message);
            } finally 
            { 

            }
            return userReturn;
        }

        public string GenerateToken(UserDTO user)
        {
            String GeneratedToken = String.Empty;
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Constants.TOKEN_SECURITY_KEY);

            var clainsToken = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Login.ToString()),
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim(ClaimTypes.UserData, StringUtils.ConvertObjectToJsonString(user)),
                    new Claim(ClaimTypes.System, user.DatabaseName),

                });
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = clainsToken,
                Expires = DateTime.UtcNow.AddHours(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            var claimsIdentity = new ClaimsIdentity(clainsToken);

            GeneratedToken =  tokenHandler.WriteToken(token);

            return GeneratedToken;
        }

        public UserDTO GetLoggedUserData(ClaimsPrincipal identity)
        {
            UserDTO userData;
            string loggedUserData= String.Empty;
            try
            {

                // Get the claims values
                loggedUserData = identity.Claims.Where(c => c.Type == ClaimTypes.UserData)
                                   .Select(c => c.Value).SingleOrDefault();
                if (String.IsNullOrEmpty(loggedUserData))
                {
                    throw new Exception("Could not retrieve username, You are not logged!");
                }
                else
                {
                    userData = (UserDTO)StringUtils.ConvertStringJsonToObject<UserDTO>(loggedUserData);
                }
            }
            catch (Exception e)
            {
                throw new ServiceException(e.Message);
            }
            return userData;
        }

        public string GetLoggedUserDatabaseName(ClaimsPrincipal identity)
        {
            string loggedUserDatabaseName = String.Empty;
            try
            {
                // Get the claims values
                loggedUserDatabaseName = identity.Claims.Where(c => c.Type == ClaimTypes.System)
                                   .Select(c => c.Value).SingleOrDefault();
                if (String.IsNullOrEmpty(loggedUserDatabaseName))
                {
                    throw new Exception("Could not retrieve User Database name, You are not logged!");
                }
                else
                {

                }
            }
            catch (Exception e)
            {
                throw new ServiceException(e.Message);
            }
            return loggedUserDatabaseName;
        }

        public long GetLoggedUserId(ClaimsPrincipal identity)
        {
            long loggedUserId = 0;
            try
            {
                // Get the claims values
                string strLoggedUserId = identity.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier)
                                   .Select(c => c.Value).SingleOrDefault();
                if (String.IsNullOrEmpty(strLoggedUserId))
                {
                    throw new Exception("Could not retrieve Id! You are not logged!");
                }
                else
                {
                    loggedUserId = Int64.Parse(strLoggedUserId);
                }
            }
            catch (Exception e)
            {
                throw new ServiceException(e.Message);
            }
            return loggedUserId;
        }

        public string GetLoggedUserName(ClaimsPrincipal identity)
        {
            string loggedUserName = String.Empty;
            try
            {

                // Get the claims values
                loggedUserName = identity.Claims.Where(c => c.Type == ClaimTypes.Name)
                                   .Select(c => c.Value).SingleOrDefault();
                if (String.IsNullOrEmpty(loggedUserName))
                {
                    throw new Exception("Could not retrieve username, You are not logged!");
                }
                else
                {

                }
            } catch(Exception e)
            {
                throw new ServiceException(e.Message);
            }
            return loggedUserName;
        }

        public ProfileMenu GetProfileMenuPermission(string DatabaseName, long ProfileId, long MenuId)
        {
            ProfileMenu permission = null;

            PersistentContext _context = new PersistentContext(DatabaseName);
            permission = _context.ProfileMenus.Where(p => p.MenuId == MenuId && p.ProfileId==ProfileId
                                                  ).FirstOrDefault();

            return permission;
        }

        public ProfileMenuFuncionality GetProfilemMenuFunctionalityPermission(string DataBaseName, long ProfileId, long MenuFunctionalityId)
        {
            ProfileMenuFuncionality permission = null;

            PersistentContext _context = new PersistentContext(DataBaseName);
            permission = _context.ProfileMenuFuncionalities.Where(p =>p.ProfileId == ProfileId && p.MenuFuncionalityId == MenuFunctionalityId
                                                  ).FirstOrDefault();

            return permission;
        }

        public UserCustommerDTO GetUserCustommerDataByLogin(string login)
        {
            UserCustommerDTO userReturn = null;
            User usSearch = GetUserByLogin(login, (long) Enums.Modules.Custommer);

            userReturn = new UserCustommerDTO()
            {
                Id = usSearch.Id,
                Login = usSearch.Login,
                Active = usSearch.Active,
                Name = usSearch.Name                           
            };

            if (usSearch.ProfileId.HasValue)
            {
                userReturn.ProfileId = usSearch.ProfileId.Value;
                userReturn.ProfileName = usSearch.Profile.Name;
            }
            else { }

            //TODO Danielg - When define rules for System Version, maybe need change the logic below to catch last version from database, instead the version from current tenant in user

            SystemVersion version = usSearch.CurrentTenant.CurrrentSystemVersion;
            userReturn.SystemVersionId = version.Id;
            userReturn.SystemVersionNumber = String.Format($"{version.VersionMajor}.{version.VersionMinor}");
            userReturn.UrlFrontEndAddress = version.UrlFrontEndAddress;
            userReturn.UrlBackEndAddress = version.UrlBackEndAddress;

            return userReturn;
        }

        public UserCustommerFullDTO AutenticateUserCustommer(string login, string password)
        {
            UserCustommerFullDTO userReturn = null;

            User usSearch = GetUserByLogin(login, (long) Enums.Modules.Custommer);

            //If have some problem when validate user password, return a exception
            CheckUserPassword(usSearch, password);

            userReturn = new UserCustommerFullDTO()
            {
                Id = usSearch.Id,
                Login = usSearch.Login,
                Active = usSearch.Active,
                Name = usSearch.Name,              
                AuthenticationDate = DateTime.Now
            };

            if (usSearch.ProfileId.HasValue)
            {
                userReturn.ProfileId = usSearch.ProfileId.Value;
                userReturn.ProfileName = usSearch.Profile.Name;
            }
            
            //TODO Danielg - When define rules for System Version, maybe need change the logic below to catch last version from database, instead the version from current tenant in user

            SystemVersion version = usSearch.CurrentTenant.CurrrentSystemVersion;
            userReturn.SystemVersionId = version.Id;
            userReturn.SystemVersionNumber = String.Format($"{version.VersionMajor}.{version.VersionMinor}");
            userReturn.UrlFrontEndAddress = version.UrlFrontEndAddress;
            userReturn.UrlBackEndAddress = version.UrlBackEndAddress;

            userReturn.DatabaseName = usSearch.CurrentTenant.DatabaseName;
            userReturn.CurrentTenantId = usSearch.CurrentTenantId;
            userReturn.TenantName = usSearch.CurrentTenant.Name;

            return userReturn;
        }

        public bool CheckUserPassword(User usSearch, string Password)
        {
            if (String.IsNullOrEmpty(Password))
            {
                throw new Exception("Enter user Password");
            }
            else { }

            string hashedGivenPassword = CryptoUtil.EncryptText(Password);
            if (hashedGivenPassword.Equals(usSearch.Password))
            {
                return true;
            } else
            {
                throw new ServiceException("Invalid Password");
            }
        }

        public List<Menu> GetMenusForProfileLoggedUser(long ProfileId)
        {
            PersistentContext _context = new PersistentContext();
            List<Menu> menus = _context.ProfileMenus.Where(p => p.ProfileId == ProfileId && p.View).Select(_=> _.Menu).ToList();
            return menus;
        }
    }
}
