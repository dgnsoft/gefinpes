﻿
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Domain.BLL.Interfaces;
using Domain.Core.Exceptions;
using Domain.Core.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.BLL
{
    public class AwsSesEmailService : IEmailServiceProvider
    {
        public bool SendEmail(string subject, string content, List<string> toAddresses)
        {

            string fromAddress = StringUtils.GetConfigurationByKey("AwsSESConfig:FromAddress");

            try
            {
                AmazonSimpleEmailServiceClient amzClient = this.GetEmailClient();

                Destination dest = new Destination();
                dest.ToAddresses.AddRange(toAddresses);                
                string subjectEmail = subject;
                Body bdy = new Body();
                bdy.Html = new Amazon.SimpleEmail.Model.Content(content);
                Amazon.SimpleEmail.Model.Content title = new Amazon.SimpleEmail.Model.Content(subjectEmail);
                Message message = new Message(title, bdy);
                SendEmailRequest ser = new SendEmailRequest(fromAddress, dest, message);

                SendEmailResponse response = amzClient.SendEmail(ser);
               
                if(response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    return true;
                } else
                {
                    return false;
                }               
            } catch(Exception e)
            {
                throw new ServiceException($"Ocorreu um erro no envio do email! Erro: {e.Message}");
            }
        }

        public AmazonSimpleEmailServiceClient GetEmailClient()
        {
            string userId = StringUtils.GetConfigurationByKey("AwsSESConfig:UserId");
            string AKID = StringUtils.GetConfigurationByKey("AwsSESConfig:AKID");
            string SECRET = StringUtils.GetConfigurationByKey("AwsSESConfig:SECRET");
            
            AmazonSimpleEmailServiceConfig amConfig = new AmazonSimpleEmailServiceConfig();
            amConfig.RegionEndpoint = Amazon.RegionEndpoint.USEast1;

            //amConfig.UseSecureStringForAwsSecretKey = false;
            AmazonSimpleEmailServiceClient amzClient = new AmazonSimpleEmailServiceClient(
                AKID,
                SECRET,
                amConfig);


            return amzClient;
        }

    }
   
}
