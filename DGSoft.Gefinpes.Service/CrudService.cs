﻿using Domain.BLL;
using Domain.BLL.Interfaces;
using Domain.DAL.Repository;
using Domain.Core.Exceptions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using Domain.BLL.Intefaces;
using Domain.Models.DTO;
using Domain.DAL.Repository.Interfaces;

namespace Domain.DAL.Service
{
    /*public class CrudService<TEntity, TSearchTO, TRepository> : ICrudService<TEntity, TSearchTO>
         where TEntity : class, IEntity
         where TSearchTO: class
         where TRepository : class, IGenericRepository<TEntity>
    {
        private readonly PersistentContext _context;
        private readonly string _databaseName;
        private UserDTO _userLoggedData { get; set; }
        //Colocar alguma forma de pegar da sessão o nome da base de dados
        public CrudService(string database)
        {
            PersistentContext _context = new PersistentContext(database);
            this._databaseName = database;
            this._context = _context;            
        }

        public void Create(TEntity entity)
        {            
            try
            {
                _context.Set<TEntity>().Add(entity);
                _context.SaveChangesWithAudit(this._userLoggedData);
            }
            catch (Exception e)
            {
                throw new ServiceException(e.Message);
            }
            finally { }
        }

        public void Delete(TEntity entity)
        {
            try
            {
                _context.Set<TEntity>().Remove(entity);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new ServiceException(e.Message);
            }
            finally { }
        }

        public void DeleteById(long id)
        {
            try
            {
                var entityToDelete = _context.Set<TEntity>().FirstOrDefault(e => e.Id == id);
                if (entityToDelete != null)
                {
                    _context.Set<TEntity>().Remove(entityToDelete);
                }
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new ServiceException(e.Message);
            }
            finally { }
        }

        public void Edit(long id,TEntity entity)
        {
            try
            {
                //Return ServiceException if no exists
                TEntity testEdit = GetById(id);
                _context.Entry(testEdit).State = EntityState.Detached;

                if (testEdit == null)
                {
                    throw new Exception($"Entity with id {id} not found");
                } else
                {
                    IAuditEntity entityOld = (IAuditEntity) testEdit;
                    ((IAuditEntity)entity).CreatedAt = entityOld.CreatedAt;
                    ((IAuditEntity)entity).CreatedBy = entityOld.CreatedBy;
                    ((IAuditEntity)entity).CreatedById = entityOld.CreatedById;
                }
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChangesWithAudit(this._userLoggedData);
            }
            catch (Exception e)
            {
                throw new ServiceException(e.Message);
            } 
            finally { }
        }

        public TEntity GetById(long id)
        {
            try
            {
                TEntity entity = _context.Set<TEntity>().FirstOrDefault(e => e.Id == id);
                if (entity != null)
                {
                    return entity;
                } else
                {
                    throw new DALException($"No data found with Id {id}!");
                }
                
            }
            catch (Exception e)
            {
                throw new ServiceException(e.Message);
            }
            finally { }
        }

        public IEnumerable<TEntity> Filter()
        {
            try
            {
                return _context.Set<TEntity>();
            }
            catch (Exception e)
            {
                throw new ServiceException(e.Message);
            }
            finally { }
        }

        public IEnumerable<TEntity> Filter(Func<TEntity, bool> predicate)
        {
            try
            {
                return _context.Set<TEntity>().Where(predicate);
            }
            catch (Exception e)
            {
                throw new ServiceException(e.Message);
            }
            finally { }
        }

        public void SaveChanges() => _context.SaveChanges();

        public void SetUserLoggedData(UserDTO UserLoggedData)
        {
            this._userLoggedData = UserLoggedData;
        }

        public IEnumerable<TEntity> Search(TSearchTO searchDTO)
        {
            throw new NotImplementedException();
            List<TEntity> result = new List<TEntity>();
           
            var repo = Activator.CreateInstance<TRepository>();
            result = repo.search(this._databaseName, searchDTO);
            return result;
        }
    }*/
}