﻿using Domain.BLL.Interfaces;
using Domain.Core;
using Domain.Core.Exceptions;
using Domain.Core.Util;
using Domain.DAL;
using Domain.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Domain.BLL
{
    public class DBService : IDBService
    {
        public bool ExecuteScript(string scriptName, string dbName)
        {
            var path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location.Substring(0, Assembly.GetEntryAssembly().Location.IndexOf("bin\\")));
            string pathScript = path + Constants.PATH_TO_SEED_SQL_FILES + scriptName;

            bool result = false;
            string custommerConnection = StringUtils.GetConfigurationByKey($"ConnectionStrings:{Constants.CUSTOMMER_STRING_DEFAULT}");

            string sqlScript = String.Empty;

            SqlConnection tmpConn;
            try
            {
                tmpConn = new SqlConnection(custommerConnection);

                if (!File.Exists(pathScript))
                {
                    throw new ServiceException("Script file to execute not found!");
                }
                else
                {
                    sqlScript = File.ReadAllText(pathScript);
                    
                    if (string.IsNullOrEmpty(sqlScript))
                    {
                        throw new ServiceException("Seed Script file not found!");
                    }
                    else
                    {
                        sqlScript = Regex.Replace(sqlScript, @"GO\r\n", String.Empty);
                        
                        using (tmpConn)
                        {
                            try
                            {

                                tmpConn.Open();
                                tmpConn.ChangeDatabase(dbName);

                                //set parameters
                                SqlCommand command = new SqlCommand(sqlScript, tmpConn);

                                int affected = command.ExecuteNonQuery();

                            }
                            catch (Exception e)
                            {
                                throw e;
                            }
                            finally
                            {
                                tmpConn.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ServiceException($"An Error ocurred when try execute script on the database! Error: {ex.Message}");
            }
            return result;
        }
    

        public bool ExecuteTenantCreationDb(Tenant tenant)
        {
            return false;
        }

        public bool IsDatabaseCreated(string dbName)
        {
            bool IsDatabaseCreated = false;
            string custommerConnection = StringUtils.GetConfigurationByKey($"ConnectionStrings:{Constants.CUSTOMMER_STRING_DEFAULT}");

            string sqlCreateDBQuery;

            SqlConnection tmpConn;
            try
            {
                tmpConn = new SqlConnection(custommerConnection);

                sqlCreateDBQuery = $"SELECT CAST(CASE WHEN db_id('{dbName}') is not null THEN 1 ELSE 0 END AS BIT)";

                using (tmpConn)
                {
                    tmpConn.Open();
                    tmpConn.ChangeDatabase("master");

                    using (SqlCommand sqlCmd = new SqlCommand(sqlCreateDBQuery, tmpConn))
                    {
                        object resultObj = sqlCmd.ExecuteScalar();

                        if (resultObj != null)
                        {
                            bool.TryParse(resultObj.ToString(), out IsDatabaseCreated);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ServiceException($"An Error ocurred when try to check if Database exists! Error: {ex.Message}");
            }
            return IsDatabaseCreated;
        }
        public bool IsDatabaseLastVersion(string dbName)
        {
            bool res = true;
            PersistentContext _context = new PersistentContext(dbName);

            try
            {
                var pendingMigrations = _context.Database.GetPendingMigrations().ToList();
                if (pendingMigrations.Count > 0)
                {
                    res = false;
                }
                else { }
            }
            catch (Exception ex)
            {
                throw new ServiceException(ex.Message);
            }
            finally { }

            return res;
        }

        public bool MigrateToLastVersion(string dbName)
        {
            bool res = false;
            PersistentContext _context = new PersistentContext(dbName);

            try
            {
                if (!IsDatabaseLastVersion(dbName))
                {
                    _context.Database.Migrate();
                    res = true;
                }
                else { }
            }
            catch (Exception ex)
            {
                throw new ServiceException(ex.Message);
            }
            finally { }

            return res;
        }

        public bool CreateDatabaseByScript(string dbName)
        {
            bool IsDatabaseCreated = false;
            string custommerConnection = StringUtils.GetConfigurationByKey($"ConnectionStrings:{Constants.CUSTOMMER_STRING_DEFAULT}");

            string sqlCreateDBQuery;

            SqlConnection tmpConn;
            try
            {
                tmpConn = new SqlConnection(custommerConnection);

                sqlCreateDBQuery = $"create database {dbName}";

                using (tmpConn)
                {
                    tmpConn.Open();
                    tmpConn.ChangeDatabase("master");

                    using (SqlCommand sqlCmd = new SqlCommand(sqlCreateDBQuery, tmpConn))
                    {
                        object resultObj = sqlCmd.ExecuteScalar();

                        if (resultObj != null)
                        {
                            bool.TryParse(resultObj.ToString(), out IsDatabaseCreated);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ServiceException($"An Error ocurred when try create Database exists! Error: {ex.Message}");
            }
            return IsDatabaseCreated;
        }

        /// <summary>
        /// See initial Data for Custommer tenant
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="SqlCreateVersionDatabase"></param>
        /// <param name="userData"></param>
        /// <returns></returns>
        public bool SeedInitialCustommerData(string dbName, string SqlCreateVersionDatabase, User userData)
            {
                var path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location.Substring(0, Assembly.GetEntryAssembly().Location.IndexOf("bin\\")));
                string pathScript = path + Constants.PATH_TO_SEED_SQL_FILES + SqlCreateVersionDatabase;

                bool result = false;
                string custommerConnection = StringUtils.GetConfigurationByKey($"ConnectionStrings:{Constants.CUSTOMMER_STRING_DEFAULT}");

                string sqlScript = String.Empty;

                SqlConnection tmpConn;
                try
                {
                    tmpConn = new SqlConnection(custommerConnection);

                    if (!File.Exists(pathScript))
                    {
                        throw new ServiceException("Seed Script file not found!");
                    }
                    else
                    {
                        sqlScript = File.ReadAllText(pathScript);
                        if (string.IsNullOrEmpty(sqlScript))
                        {
                            throw new ServiceException("Seed Script file not found!");
                        }
                        else
                        {
                            using (tmpConn)
                            {
                                try
                                {

                                    tmpConn.Open();
                                    tmpConn.ChangeDatabase(dbName);

                                    //set parameters
                                    SqlCommand command = new SqlCommand(sqlScript, tmpConn);
                                    command.Parameters.AddWithValue("@userName", userData.Name);
                                    command.Parameters.AddWithValue("@userLogin", userData.Login);
                                    command.Parameters.AddWithValue("@userPassword", userData.Password);
                                    command.Parameters.AddWithValue("@externalAdminUserId", userData.Id);

                                    int affected = command.ExecuteNonQuery();

                                }
                                catch (Exception e)
                                {
                                    throw e;
                                }
                                finally
                                {
                                    tmpConn.Close();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ServiceException($"An Error ocurred when try seed the database! Error: {ex.Message}");
                }
                return result;
            }

        /// <summary>
        /// Get Last SystemVersion for a module in a Status
        /// </summary>
        /// <param name="ModuleId"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        public SystemVersion GetLastCurrentSystemVersion(long ModuleId, Enums.SystemVersionStatus Status)
        {
            SystemVersion versionReturn = null;

            PersistentContext _context = new PersistentContext();
            try
            {
                List<SystemVersion> systemVersions = _context.SystemVersions.Where(p => p.SystemVersionStatusId == (long)Status && p.ModuleId == ModuleId).ToList();
                systemVersions = systemVersions.OrderByDescending(p => p.FullVersionCode).ToList();
                versionReturn = systemVersions.FirstOrDefault();
                
            } catch(Exception e)
            {
                throw new ServiceException($"Version not Found for Module {ModuleId} and Status Version {Status.ToString()}");

            }
            return versionReturn;
        }
    }
}
