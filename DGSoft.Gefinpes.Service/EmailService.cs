﻿using Domain.BLL.Interfaces;
using Domain.Core;
using Domain.Core.Exceptions;
using Domain.Core.Util;
using Domain.DAL;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using static Domain.Core.Enums;

namespace Domain.BLL
{
    /// <summary>
    /// Service to send emails
    /// </summary>
    public class EmailService : IMessageService
    {
        /// <summary>
        /// Method to Sent an Email. EmailProvider is configured on appSetttings. 
        /// Emails could be sent by AwsSesEmailService or SmtpEmailService
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="content"></param>
        /// <param name="toAddresses"></param>
        /// <returns></returns>
        public bool Send(string subject, string content, List<string> toAddresses)
        {
            try
            {
                string defaultEmailServiceProvider = StringUtils.GetConfigurationByKey("DefaultEmailServiceProvider");
                if (string.IsNullOrEmpty(defaultEmailServiceProvider))
                {
                    throw new ServiceException(@"No Default Email Service configured on AppSettings! 
                                                Please insert Tag on config File with the name of Default Email Service Provider as implemented.");
                }
                else
                {
                    var service = ReflectionUtils.GetInstance($"Domain.BLL.{defaultEmailServiceProvider}");

                    if (service != null)
                    {
                        IEmailServiceProvider _emailService = (IEmailServiceProvider)service;
                        bool result = _emailService.SendEmail(subject, content, toAddresses);
                        return result;
                    }
                    else
                    {
                        throw new Exception($"Email provider {defaultEmailServiceProvider} not Found ");
                    }
                }
            }
            catch (Exception e)
            {
                throw new ServiceException($"An error ocurred when try to sent Email! Error: {e.Message}");
            }
        }

        public Message CreateNewMessage(string subject, string content, List<string> toAddresses, DateTime dtAction, string userCreated, long? userCreatedId = 0)
        {
            Message message = new Message((long)MessageTypes.Email);
            PersistentContext _db = new PersistentContext();

            try
            {

                if (String.IsNullOrEmpty(subject))
                {
                    throw new ServiceException($"Subject for Email Message is Mandatory");
                }
                else if (String.IsNullOrEmpty(content))
                {
                    throw new ServiceException($"Content for Email Message is Mandatory");
                }
                else if(toAddresses.Count==0){
                    throw new ServiceException($"At least on Address for Email Message is Mandatory");
                }

                message.Subject = subject;
                message.Content = content;
                message.Destination = String.Join(Constants.MESSAGE_EMAIL_DEST_SEPARATOR, toAddresses);
                
                message.CreatedAt = dtAction;
                message.CreatedBy = userCreated;
                message.CreatedById = userCreatedId.GetValueOrDefault();

                message.UpdatedAt = dtAction;
                message.UpdatedBy = userCreated;
                message.UpdatedById = userCreatedId.GetValueOrDefault();

                _db.Messages.Add(message);
                _db.SaveChanges();

            } catch (Exception e)
            {
                throw new ServiceException($"An error ocurred when try to Create Email! Error: {e.Message}");
            }
            return message;
        }
    }
}
