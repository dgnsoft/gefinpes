﻿using Domain.BLL;
using Domain.BLL.Interfaces;
using Domain.DAL.Repository;
using Domain.Core.Exceptions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using Domain.BLL.Intefaces;
using Domain.Models.DTO;
using Domain.DAL.Repository.Interfaces;
using DGSoft.Gefinpes.Domain.Enums;
using Amazon.IdentityManagement.Model;
using DGSoft.Gefinpes.Domain.Models.Core;
using System.Linq.Expressions;
using static DGSoft.Gefinpes.Domain.Enums.Response;
using DGSoft.Gefinpes.Utility;
using Domain.Models;

namespace Domain.DAL.Service
{
    public abstract class GenericService<T> : IGenericService<T> where T : EntityBase
    {
        private readonly PersistentContext _context;
        private readonly string _databaseName;
        protected IGenericRepository<T> repository;

        private UserDTO _userLoggedData { get; set; }
        //Colocar alguma forma de pegar da sessão o nome da base de dados
        public GenericService()
        {
            PersistentContext _context = new PersistentContext();
            repository = new GenericRepository<T>(_context);
            //this._databaseName = database;
            this._context = _context;
        }

        public virtual ResponseModel<T> GenerateResponse(List<T> Result, string Message = null, bool Success = true, ExceptionLevel ExceptionLevel = ExceptionLevel.None, ExceptionType ExceptionType = ExceptionType.None)
        {

            ResponseModel<T> resp = new  ResponseModel<T>()
            {
                Result = Result,
                Message = new List<string>(),
                Success = Success,
                ExceptionLevel = ExceptionLevel,
                ExceptionType = ExceptionType
            };
            resp.Message.Add(Message);
            return resp;
        }

        public virtual ResponseModel<T> GenerateResponseError(List<T> Result, List<string> Message = null, List<string> Errors = null, bool Success = true, ExceptionLevel ExceptionLevel = ExceptionLevel.None, ExceptionType ExceptionType = ExceptionType.None)
        {

            ResponseModel<T> resp = new ResponseModel<T>()
            {
                Result = Result,
                Message = Message,
                Errors = Errors,
                Success = Success,
                ExceptionLevel = ExceptionLevel,
                ExceptionType = ExceptionType
            };            
            return resp;
        }

        public virtual ResponseModel<T> Create(T entity)
        {
            try
            {
                List<T> res = new List<T>();
                res.Add(repository.Create(entity, _userLoggedData));
                return GenerateResponse(res);
            }
            catch (Exception e)
            {
                throw new ServiceException($"Error when save Data {typeof(T)}. Detail: {GetExMessageWithInerrMessage(e)}.");
            }
            finally { }
        }

        public string GetExMessageWithInerrMessage(Exception e)
        {
            string erro = String.Empty;
            if (e != null)
            {
                erro += e.Message;
            }
            if (e.InnerException != null && !string.IsNullOrEmpty(e.InnerException.Message))
            {
                erro += $" / " + e.InnerException.Message;
            }
            return erro;
        }

        public ResponseModel<T> Delete(T entity)
        {
            try
            {
                repository.Delete(entity, _userLoggedData);
                return GenerateResponse(null);
            }
            catch (Exception e)
            {
                throw new ServiceException($"Error when save Data {typeof(T)}. Detail: {GetExMessageWithInerrMessage(e)}.");
            }
            finally { }
        }

        public ResponseModel<T> DeleteById(long id)
        {
            try
            {
                repository.DeleteById(id, _userLoggedData);
                return GenerateResponse(null);
            }
            catch (Exception e)
            {
                throw new ServiceException($"Error when save Data {typeof(T)}. Detail: {GetExMessageWithInerrMessage(e)}.");
            }
            finally { }
        }

        public ResponseModel<T> Edit(long id, T entity)
        {
            try
            {
                List<T> res = new List<T>();
                res.Add(repository.Edit(id, entity, _userLoggedData));
                return GenerateResponse(res);
            }
            catch (Exception e)
            {
                throw new ServiceException(e.Message);
            }
            finally { }
        }

        public ResponseModel<T> GetById(long id)
        {
            try
            {
                List<T> res = new List<T>();
                T entity = repository.GetById(id);
                if (entity == null)
                {
                    string message = $"{typeof(T).Name} with id {id} not found.";
                    List<string> teste = new List<string> { message, message };
                    return GenerateResponseError(null, null, teste, false);
                } else
                {
                    res.Add(entity);
                    return GenerateResponse(res);
                }
            }
            catch (Exception e)
            {
                throw new ServiceException(e.Message);
            }
            finally { }
        }

        public ResponseModel<T> Filter()
        {
            try
            {              
                return GenerateResponse(repository.Filter().ToList()) ;
            }
            catch (Exception e)
            {
                throw new ServiceException(e.Message);
            }
            finally { }
        }

        public ResponseModel<T> Filter(Expression<Func<T, bool>> expression)
        {
            try
            {
                return GenerateResponse(repository.Filter(expression).ToList());
            }
            catch (Exception e)
            {
                throw new ServiceException(e.Message);
            }
            finally { }
        }

    }
}