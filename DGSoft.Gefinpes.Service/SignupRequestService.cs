﻿using Domain.Core.Util;
using Domain.DAL;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Domain.Core;
using static Domain.Core.Enums;
using Domain.Core.Exceptions;
using Domain.BLL.Interfaces;

namespace Domain.BLL
{
    public class SignupRequestService : ISignupRequestService
    {

        private readonly IDBService _dBService;
        

        public SignupRequestService(IDBService dbSevice)
        {
            this._dBService = dbSevice;
        }

        public void ValidateSingupData(SignupRequest signup, PersistentContext db)
        {
            if (!StringUtils.IsValidEmail(signup.EmailAddress))
            {
                throw new ServiceException($"Informe um endereço de email válido!");
            }
            else { }
            
            if (StringUtils.IsValidPassword(signup.Password))
            {
                throw new ServiceException($"Senha Inválida! A Senha deve entre {Constants.MIN_CHAR_PASSWORD} e {Constants.MAX_CHAR_PASSWORD} caracteres,  entre letras e Números.");
            } 
            else { }

            if (db.SignupRequests.Where(p => p.EmailAddress == signup.EmailAddress && p.SignupRequestStatus != SignupRequestStatus.Canceled).Count() > 0)
            {
                throw new ServiceException($"Já existe uma solicitação de signup cadastras com este email {signup.EmailAddress}");
            }
            else { }

            if (db.Users.Where(p => p.Login.ToUpper() == signup.EmailAddress.ToUpper()).Count() > 0)
            {
                throw new ServiceException($"Já existe um usuário cadastrado com este login {signup.EmailAddress} no sistema.");
            }
            else { }
        }

        public SignupRequest Create(SignupRequest signup)
        {
            try
            {
                DateTime dateAction = DateTime.Now;
                PersistentContext db = new PersistentContext();

                EmailService emailService = new EmailService();

                ValidateSingupData(signup, db);


                //Generate SignupRequest
                string signupKey = GenerateSignupKey(db);

                if (!String.IsNullOrEmpty(signupKey))
                {
                    signup.SingupRequestKey = signupKey;
                    string hashedPassword = CryptoUtil.EncryptText(signup.Password);
                    signup.Password = hashedPassword;
                    signup.CreatedAt = dateAction;
                    signup.UpdatedAt = dateAction;
                    signup.CreatedBy = Constants.USER_SYSTEM_ACTIONS;
                    signup.UpdatedBy = Constants.USER_SYSTEM_ACTIONS;

                    db.SignupRequests.Add(signup);
                    db.SaveChanges();

                    //Create email to sent to user
                    String emailContent = GetSignupEmailContent(signup);

                    List<string> toAddresses = new List<string>()
                {
                    signup.EmailAddress
                };

                    Message email = emailService.CreateNewMessage(Constants.TITLE_SIGNUP_EMAIL,
                                                  emailContent,
                                                  toAddresses,
                                                  dateAction,
                                                  Constants.USER_SYSTEM_ACTIONS
                                                  );

                }
                else { }

                return signup;
            } catch(Exception e)
            {
                throw e;
            }            
        }

        public String GetSignupEmailContent(SignupRequest request)
        {
            string linkConfirm = StringUtils.GetConfigurationByKey("SignupLinkEfetivation") + request.SingupRequestKey;

            StringBuilder sb = new StringBuilder();
            sb.Append($"<h3>Olá <u>{request.Name}</u></h3>");
            sb.Append("<br/><br/>");
            sb.Append("Recebemos seu pedido de cadastro para utilizar nossos serviçoes. Falta pouco pra vc ter acesso a nosso portal.");
            sb.Append("<br/><br/>");
            sb.Append($"Clique <a href=\"{linkConfirm}\">aqui</a> para efetivar o cadastro.");
            sb.Append("<br/><br/>");
            sb.Append($"Ou copie e cole na barra de endereços do navegador o endereço abaixo: <br/>");
            sb.Append(linkConfirm);
            sb.Append("<br/></br>Após a Confirmação você receberá um email com o link para acessar a aplicação!");
            sb.Append("</br></br>");

            return sb.ToString();
        }

        public String GetSignupConfirmationSuccessEmailContent(SignupRequest request, string urlApp)
        {

            StringBuilder sb = new StringBuilder();
            sb.Append($"<h3>Olá <u>{request.Name}</u></h3>");
            sb.Append("<br/><br/>");
            sb.Append("Seu pedido de Cadastro foi finalizado com sucesso!.");
            sb.Append("<br/><br/>");
            sb.Append($"Clique <a href=\"{urlApp}\">aqui</a> para efetuar o login e acessar a aplicação");
            sb.Append("<br/><br/>");
            sb.Append($"Ou copie e cole na barra de endereços do navegador o endereço abaixo: <br/>");
            sb.Append(urlApp);            
            sb.Append("</br></br>");

            return sb.ToString();
        }

        public String GetSignupConfirmationErrorEmailContent(SignupRequest request)
        {

            StringBuilder sb = new StringBuilder();
            sb.Append($"<h3>Olá <u>{request.Name}</u></h3>");
            sb.Append("<br/><br/>");
            sb.Append("Ocorreu um erro ao confirmar o cadastro no Portal!.");
            sb.Append("<br/><br/>");
            //
            return sb.ToString();
        }


        public string GenerateSignupKey(PersistentContext db)
        {
            DateTime date = DateTime.Now;
            string HashedKey = String.Empty;
        
            bool generateNewHash = true;

            while (generateNewHash)
            {
                date = DateTime.Now;
                long ticks = date.Ticks;
                HashedKey =ticks.ToString("X");

                if(db.SignupRequests.Where(p => p.SingupRequestKey == HashedKey).Count() == 0)
                {
                    generateNewHash = false;
                }
                else { }
            }

            return HashedKey;
        }

        //TODO - Rotina para invalidar Signup que passar de X Dias. Ver com abdalla.
        public bool ConfirmSignup(string signupKey)
        {
            DateTime dateAction = DateTime.Now;
            
            PersistentContext _context = new PersistentContext();


            bool result = false;
            bool signupProcessed = true;

            if (String.IsNullOrEmpty(signupKey))
            {
                throw new ServiceException($"No Signup request found with the key:  {signupKey}!");
            }
            else
            {
                try
                {
                    SignupRequest signup = _context.SignupRequests.Where(p => p.SingupRequestKey == signupKey && p.SignupRequestStatus == SignupRequestStatus.Pending).FirstOrDefault();
                    Tenant tenantToCreate = new Tenant();
                    User userToCreate = new User();

                    SystemVersion lastSystemVersion = new SystemVersion();
                    if (signup == null)
                    {
                        throw new Exception($"Signup with key {signupKey} is not Pending.");
                    } else
                    {
                        //Get last SistemVersion
                        lastSystemVersion = this._dBService.GetLastCurrentSystemVersion((long)Enums.Modules.Custommer, Enums.SystemVersionStatus.Production);

                        signup.SignupRequestStatus = SignupRequestStatus.InProcess;
                        _context.Entry(signup).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                        _context.SaveChanges();

                        string errorMessage = String.Empty;
                        //CreateUser in admin
                        using(var tran = _context.Database.BeginTransaction())
                        {
                            //pegar erra caso dê problema no meio do caminho
                            try
                            {
                                userToCreate = new User()
                                {
                                    Active = true,
                                    Login = signup.EmailAddress,
                                    Password = signup.Password,
                                    Name = signup.Name,
                                    ChangePassword = false,
                                    ModuleId = (int)Enums.Modules.Custommer,
                                    UserType = 2,
                                    CreatedAt = dateAction,
                                    CreatedBy = Constants.USER_SYSTEM_ACTIONS,
                                    CreatedById = 0
                                };

                                _context.Users.Add(userToCreate);
                                _context.SaveChanges();

                                //Create Tenant
                                tenantToCreate = new Tenant()
                                {
                                    CreatedAt = dateAction,
                                    CreatedBy = Constants.USER_SYSTEM_ACTIONS,
                                    CreatedById = 0,
                                    Name = signup.TradingName,
                                    DatabaseName = Constants.DB_TENTANT_PREFIX,
                                    AdminUserId = userToCreate.Id,
                                    CurrrentSystemVersionId = lastSystemVersion.Id
                                };

                                _context.Tenants.Add(tenantToCreate);
                                _context.SaveChanges();

                                //Genetare DBName for tenant db_tXXX where XXX is tenantId
                                tenantToCreate.DatabaseName = $"{Constants.DB_TENTANT_PREFIX}{tenantToCreate.Id.ToString("0000")}";
                                _context.Entry(tenantToCreate).State = Microsoft.EntityFrameworkCore.EntityState.Modified;                                

                                //set current tenant on user
                                userToCreate.CurrentTenantId = tenantToCreate.Id;
                                _context.Entry(userToCreate).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                                
                                _context.SaveChanges();

                                //Create Custommer 
                                Customer custommer = new Customer()
                                {
                                    Active = true,
                                    IndividualRegistration = signup.IndividualRegistration,
                                    Name = signup.Name,
                                    TradingName = signup.TradingName,
                                    TenantiId = tenantToCreate.Id
                                };

                                _context.Customers.Add(custommer);
                                _context.SaveChanges();

                                tran.Commit();
                            } catch(Exception e)
                            {
                                signupProcessed = false;
                                errorMessage = e.Message;
                                
                                tran.Rollback();
                            }
                            SignupRequestStatus finishStatusSignup ;

                            if (signupProcessed)
                            {
                                finishStatusSignup = SignupRequestStatus.Processed;
                                this._dBService.CreateDatabaseByScript(tenantToCreate.DatabaseName);
                                
                                this._dBService.ExecuteScript(lastSystemVersion.SqlCreateVersionDatabase, tenantToCreate.DatabaseName);
                                this._dBService.SeedInitialCustommerData(tenantToCreate.DatabaseName, lastSystemVersion.SqlUpdateVersionDatabase, userToCreate);
                                
                                SendSucessEmail(signup, Constants.URL_LOGIN_CUSTOMMER);

                            } else {
                                finishStatusSignup = SignupRequestStatus.Error;
                                SendErrorSignupEmail(signup, Constants.URL_LOGIN_CUSTOMMER);
                            }
                            signup.SignupRequestStatus = finishStatusSignup;
                           _context.Entry(signup).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                            _context.SaveChanges();
                        }
                      
                    }

                    result = signupProcessed;
                } catch(Exception e)
                {
                    throw new ServiceException($"An error ocurred when try to efefivate SignUp with key {signupKey}! Error: {e.Message}.");
                }
            }
            return result;
        }

        public void SendSucessEmail(SignupRequest signup, string UrlFrontEndAddress)
        {

            IMessageService emailService = new EmailService();
            DateTime dateAction = DateTime.Now;

            //Create email to sent to user
            String emailContent = GetSignupConfirmationSuccessEmailContent(signup, UrlFrontEndAddress);

            List<string> toAddresses = new List<string>()
                {
                    signup.EmailAddress
                };

            Message email = emailService.CreateNewMessage(Constants.TITLE_SIGNUP_EMAIL_SUCCESS,
                                          emailContent,
                                          toAddresses,
                                          dateAction,
                                          Constants.USER_SYSTEM_ACTIONS
                                          );
        }

        public void SendErrorSignupEmail(SignupRequest signup, string UrlFrontEndAddress)
        {

            IMessageService emailService = new EmailService();
            DateTime dateAction = DateTime.Now;

            //Create email to sent to user
            String emailContent = GetSignupConfirmationErrorEmailContent(signup);

            List<string> toAddresses = new List<string>()
                {
                    signup.EmailAddress
                };

            Message email = emailService.CreateNewMessage(Constants.TITLE_SIGNUP_EMAIL_ERROR,
                                          emailContent,
                                          toAddresses,
                                          dateAction,
                                          Constants.USER_SYSTEM_ACTIONS
                                          );
        }
    }
}
