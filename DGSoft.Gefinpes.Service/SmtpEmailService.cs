﻿
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Domain.BLL.Interfaces;
using Domain.Core.Exceptions;
using Domain.Core.Util;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Domain.BLL
{
    public class SmtpEmailService : IEmailServiceProvider
    {
        public const string CONFIG_EMAIL_KEY = "EmailServiceConfig";
        public bool SendEmail(string subject, string content, List<string> toAddresses)
        {
            string fromAddress = StringUtils.GetConfigurationByKey($"{CONFIG_EMAIL_KEY}:FromAddress");

            try
            {
                SmtpClient client = this.GetEmailClient();

                MailMessage message = new MailMessage();
                foreach(string email in toAddresses)
                {
                    message.To.Add(email);
                }
                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = content;

                message.From = new MailAddress(fromAddress);

                client.Send(message);
               
                return true;                          
            } catch(Exception e)
            {
                throw new ServiceException($"Ocorreu um erro no envio do email! Erro: {e.Message}");
            }
        }

        public SmtpClient GetEmailClient()
        {
            string userId = StringUtils.GetConfigurationByKey($"{CONFIG_EMAIL_KEY}:UserId");
            string password = StringUtils.GetConfigurationByKey($"{CONFIG_EMAIL_KEY}:SECRET");
            string PORT = StringUtils.GetConfigurationByKey($"{CONFIG_EMAIL_KEY}:PORT");
            string SMTP = StringUtils.GetConfigurationByKey($"{CONFIG_EMAIL_KEY}:SMTP");
            string EnableSsl = StringUtils.GetConfigurationByKey($"{CONFIG_EMAIL_KEY}:EnableSSL");

            SmtpClient client = new SmtpClient(SMTP, int.Parse(PORT));
            if (!String.IsNullOrEmpty(EnableSsl))
            {
                bool ssl = bool.Parse(EnableSsl);
                client.EnableSsl = ssl;
            }
            else { }

            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            NetworkCredential credential = new NetworkCredential(userId, password);
            client.Credentials = credential;

            return client;
        }

    }
   
}
