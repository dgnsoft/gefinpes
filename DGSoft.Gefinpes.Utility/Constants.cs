﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Core
{
    public static class Constants
    {
        public const string TOKEN_SECURITY_KEY = "PortifoliogefinpesSWB@2020";
        
        public const string REST_PREFIX_API = "api";

        public const string CONNECTION_STRING_DEFAULT = "DefaultConnection";
        public const string CUSTOMMER_STRING_DEFAULT = "CustommerConnection";

        public const string Roles = "roles";

        public const string MESSAGE_EMAIL_DEST_SEPARATOR = ";";

        public const string USER_SYSTEM_ACTIONS = "system";

        /// <summary>
        /// Api Name
        /// </summary>
        public const string ApiName = "gefinpes_api";
        /// <summary>
        /// Api Friendly name
        /// </summary>
        public const string ApiFriendlyName = "DGSoft Gefinpes - API";
        /// <summary>
        /// Quick App Client SPA Id
        /// </summary>
        public const string QuickAppClientID = "gefinpes_spa";
        /// <summary>
        /// Swagger Client ID
        /// </summary>
        public const string SwaggerClientID = "gefinpes_swaggerui";

        public const long ID_MENU_PROFILE = 2;        


        #region SingupEmail
        public const string TITLE_SIGNUP_EMAIL = "Inscrição gefinpes SWB";
        public const string TITLE_SIGNUP_EMAIL_SUCCESS = "Inscrição gefinpes SWB - Confirmada";
        public const string TITLE_SIGNUP_EMAIL_ERROR = "Inscrição gefinpes SWB - Erro";
        #endregion

        public const string PATH_TO_SEED_SQL_FILES = "\\SqlScripts\\";
        public const string PATH_TO_SEED_CUSTOMMER_SQL_FILE = "seed_custommer_db.sql";
        public const string DB_TENTANT_PREFIX = "db_t";
        public const int MIN_CHAR_PASSWORD = 8;
        public const int MAX_CHAR_PASSWORD = 20;

        public const string URL_LOGIN_CUSTOMMER = "http://gefinpes.swb.com.br:8090/login";
    }
}
