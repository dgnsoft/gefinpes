﻿using DGSoft.Gefinpes.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Text;
using static DGSoft.Gefinpes.Domain.Enums.Response;

namespace DGSoft.Gefinpes.Utility
{
   public class GenerateResponse<T>
    {
        public ResponseModel<T> Create(List<T> Result, string Message = null, bool Success = true, ExceptionLevel ExceptionLevel = ExceptionLevel.None, ExceptionType ExceptionType = ExceptionType.None)
        {

            ResponseModel<T> result = new ResponseModel<T>()
            {
                Result = Result,
                Message = new List<string>(),
                Success = Success,
                ExceptionLevel = ExceptionLevel,
                ExceptionType = ExceptionType
            };
            if (!string.IsNullOrEmpty(Message))
            {
                result.Message.Add(Message);
            }
            return result;
        }
    }
}
