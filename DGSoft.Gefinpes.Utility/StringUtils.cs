﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;

namespace Domain.Core.Util
{
    public static class StringUtils
    {
        public static string GetConfigurationByKey(string keyName)
        {
            var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .AddJsonFile("appsettings.Development.json", optional: true)
            .Build();

            string connection = configuration[keyName];
            return connection;
        }
        
        public static String getExceptionMsgCustom(Exception ex)
        {
            Exception current = ex;
            String msg = "";
            while (current != null)
            {
                msg += current.Message;
                current = current.InnerException;
            }
            return msg;
        }

        public static String GetLogProcessamento(String acao, DateTime dtInicio, DateTime dtFim)
        {
            if (dtFim == null)
            {
                dtFim = DateTime.Now; //OK
            }
            TimeSpan ts = dtFim - dtInicio;
            String ret = acao + " inciada em:  " + dtInicio.ToString() + " e finalizada em: " + dtFim.ToString() + ". Processado em: " + ts.ToString() + ".";

            return ret;
        }

        public static String GetLogProcessamento2(String acao, DateTime dtInicio, DateTime dtFim)
        {
            if (dtFim == null)
            {
                dtFim = DateTime.Now; //OK
            }
            TimeSpan ts = dtFim - dtInicio;
            String ret = acao + "/I: " + dtInicio.ToString() + " /F: " + dtFim.ToString() + ". Gasto: " + ts.ToString() + ".";

            return ret;
        }

        public static string ConvertObjectToJsonString(object obj)
        {
            var serialized = JsonConvert.SerializeObject(obj);
            return serialized;
        }

        public static Object ConvertStringJsonToObject<Type>(String str)
        {
            Type obj = JsonConvert.DeserializeObject<Type>(str);
            return obj;
        }

        public static string GetStringValueFromReader(Stream stream)
        {
            string result = String.Empty;
            if(stream!=null)
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
            }
            return result;
        }

        public static string RemoveAcentuation(string text)
        {
            return
                System.Web.HttpUtility.UrlDecode(
                    System.Web.HttpUtility.UrlEncode(
                        text, Encoding.GetEncoding("iso-8859-7")));
        }

        public static bool IsValidEmail(string source)
        {
            return new EmailAddressAttribute().IsValid(source);
        }

        public static bool IsValidPassword(string source)
        {
            bool result = true;
            if(source == null || source.Length<Constants.MIN_CHAR_PASSWORD || source.Length > Constants.MAX_CHAR_PASSWORD)
            {
                result = false;
            } else
            {
                string regex = @"^\d*[a-zA-Z][a-zA-Z\d]*$";
                Match m = Regex.Match(source, regex);
                result = m.Success;
            }
            
            return result;
        }
    }
}
